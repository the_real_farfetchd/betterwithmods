/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.container;

import com.betterwithmods.core.base.game.tile.TileBase;
import com.betterwithmods.core.utilties.PlayerUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class InventoryContainer<T extends TileBase> extends TileContainer<T> {

    protected IItemHandler inventory;

    public InventoryContainer(EntityPlayer player, T tile) {
        this(player, tile, tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).orElse(null));
    }

    public InventoryContainer(EntityPlayer player, T tile, IItemHandler inventory) {
        super(player, tile);
        this.inventory = inventory;
    }

    private static final int SLOT_WIDTH = 18;

    protected void addSlots(IItemHandler inventory, int slots, int rows, int startIndex, int startX, int startY) {
        addSlots(inventory, slots, rows, startIndex, startX, startY, SlotItemHandler::new);
    }

    protected void addSlots(IItemHandler inventory, int slots, int rows, int startIndex, int startX, int startY, SlotSupplier supplier) {
        int columns = slots / rows;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int index = j + i * columns + startIndex;
                int x = startX + j * SLOT_WIDTH;
                int y = startY + i * SLOT_WIDTH;
                this.addSlot(supplier.create(inventory, index, x, y));
            }
        }
    }

    protected void addPlayerInventory(int startIndex, int startX, int startY, int hotbarOffsetY) {
        IItemHandler inventory = PlayerUtils.getJoinedInventory(player);
        addSlots(inventory,9, 1, 0, startX, (startY + hotbarOffsetY));
        addSlots(inventory, 27, 3, 9, startX, startY);
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return false;
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        //TODO
        return ItemStack.EMPTY;
    }
}
