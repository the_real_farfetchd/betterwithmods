/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.block.plant;

import com.betterwithmods.core.utilties.BooleanOperator;
import net.minecraft.block.BlockRedstoneLamp;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReaderBase;

import java.util.Arrays;
import java.util.Collection;

public class GrowthConditions {

    public static final BooleanOperator
            OR = (a, b) -> a || b,
            AND = (a, b) -> a && b;


    public static boolean hasSun(IWorldReaderBase worldIn, BlockPos pos, IBlockState state) {
        return worldIn.canSeeSky(pos);
    }

    public static boolean hasLamp(IWorldReaderBase worldIn, BlockPos pos, IBlockState state) {
        IBlockState above = worldIn.getBlockState(pos.up(2));
        return above.getBlock() == Blocks.REDSTONE_LAMP && above.get(BlockRedstoneLamp.LIT);
    }

    public static boolean isFertileGround(IWorldReaderBase worldIn, BlockPos pos, IBlockState state) {
        return worldIn.getBlockState(pos.down()).isFertile(worldIn, pos.down());
    }

    public static IGrowthCondition aggregate(BooleanOperator reducer, Collection<IGrowthCondition> conditions) {
        return (worldIn, pos, state) -> conditions.stream().map(c -> c.canGrow(worldIn, pos, state)).reduce(reducer).orElse(false);
    }

    public static IGrowthCondition aggregate(BooleanOperator reducer, IGrowthCondition... conditions) {
        return aggregate(reducer, Arrays.asList(conditions));
    }


    public static class LightLevel implements IGrowthCondition {
        private int light;

        public LightLevel(int light) {
            this.light = light;
        }

        @Override
        public boolean canGrow(IWorldReaderBase worldIn, BlockPos pos, IBlockState state) {
            return worldIn.getLightSubtracted(pos.up(), 0) >= light;
        }
    }

}
