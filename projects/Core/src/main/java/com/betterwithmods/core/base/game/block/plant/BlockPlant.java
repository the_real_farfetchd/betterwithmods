/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.block.plant;

import com.betterwithmods.core.utilties.BooleanOperator;
import com.betterwithmods.core.utilties.Rand;
import com.google.common.collect.Lists;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.IGrowable;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReaderBase;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;

import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

public abstract class BlockPlant extends BlockBush implements IGrowable, IPlantable, IPlant {

    private static VoxelShape[] SHAPE_BY_AGE = new VoxelShape[]{
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 2.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 4.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 6.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 8.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 10.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 12.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 14.0D, 16.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 16.0D, 16.0D)
    };


    private final int maxAge;

    private final Collection<IGrowthCondition> growthConditions;
    private BooleanOperator growthConditionReducer;

    public BlockPlant(Block.Properties builder, int maxAge) {
        super(builder);
        this.maxAge = maxAge;
        this.growthConditions = Lists.newArrayList();
        this.growthConditionReducer = (a, b) -> a && b;
    }


    public BlockPlant growthConditions(IGrowthCondition... conditions) {
        this.growthConditions.addAll(Arrays.asList(conditions));
        return this;
    }

    public BlockPlant growthConditionReducer(BooleanOperator reducer) {
        this.growthConditionReducer = reducer;
        return this;
    }

    public abstract IItemProvider getSeed();

    public abstract IItemProvider getCrop();

    public abstract IntegerProperty getAgeProperty();

    protected int getMaxAge() {
        return maxAge;
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        builder.add(getAgeProperty());
    }

    @SuppressWarnings("deprecation")
    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        return SHAPE_BY_AGE[state.get(this.getAgeProperty())];
    }

    protected int getAge(IBlockState state) {
        return state.get(this.getAgeProperty());
    }

    protected boolean isMaxAge(IBlockState state) {
        return state.get(this.getAgeProperty()) >= this.getMaxAge();
    }

    protected IBlockState withAge(int age) {
        return this.getDefaultState().with(this.getAgeProperty(), age);
    }

    @Override
    public boolean canGrow(IBlockReader worldIn, BlockPos pos, IBlockState state, boolean isClient) {
        if (isMaxAge(state))
            return false;
        return isSatisfied((IWorldReaderBase) worldIn, pos, state);
    }

    @Override
    public boolean canUseBonemeal(World worldIn, Random rand, BlockPos pos, IBlockState state) {
        return true;
    }

    @Override
    public void grow(World worldIn, Random rand, BlockPos pos, IBlockState state) {
        int newAge = Math.min(this.getAge(state) + 1, getMaxAge());
        worldIn.setBlockState(pos, withAge(newAge), 2);
    }

    @Override
    public EnumPlantType getPlantType(IBlockReader world, BlockPos pos) {
        return EnumPlantType.Crop;
    }

    public boolean isSatisfied(IWorldReaderBase worldIn, BlockPos pos, IBlockState state) {
        return GrowthConditions.aggregate(growthConditionReducer, growthConditions).canGrow(worldIn, pos, state);
    }

    @Override
    protected boolean isValidGround(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        return state.getBlock().isFertile(state, worldIn, pos);
    }

    @Override
    public void getDrops(IBlockState state, net.minecraft.util.NonNullList<ItemStack> drops, World world, BlockPos pos, int fortune) {
        drops.add(new ItemStack(this.getSeed()));

        if (isMaxAge(state)) {
            if (Rand.chance(world.rand, 0.3)) {
                drops.add(new ItemStack(this.getSeed()));
            }
            drops.add(new ItemStack(getCrop()));
        }
    }


    @Override
    public void tick(IBlockState state, World worldIn, BlockPos pos, Random random) {
        super.tick(state, worldIn, pos, random);
        if (!worldIn.isAreaLoaded(pos, 1))
            return; // Forge: prevent loading unloaded chunks when checking neighbor's light
        if (canGrow(worldIn, pos, state, false)) {
            boolean chance = shouldGrow(worldIn, pos);
            if (net.minecraftforge.common.ForgeHooks.onCropsGrowPre(worldIn, pos, state, chance)) {
                this.grow(worldIn, random, pos, state);
                net.minecraftforge.common.ForgeHooks.onCropsGrowPost(worldIn, pos, state);
            }
        }
    }

    public boolean shouldGrow(World worldIn, BlockPos pos) {
        return Rand.chance(worldIn.rand, 0.2);
    }

}
