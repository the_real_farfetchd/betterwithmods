package com.betterwithmods.core.base.game.movingstructure;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.UUID;

@ParametersAreNonnullByDefault
public class MovingStructure {

    private final World world;
    private final UUID uuid;

    private Vec3d pos;
    private Vec3d size;

    private Callback callback;

    private Path currentPath;
    private long lastPathFinished;

    public MovingStructure(World world, UUID uuid) {
        this.world = world;
        this.uuid = uuid;
    }

    public Vec3d getPos() {
        return pos.add(currentPath.getOffset((int) (lastPathFinished - world.getGameTime())));
    }

    public Vec3d getBasePos() {
        return pos;
    }

    public Vec3d getSize() {
        return size;
    }

    public UUID getId() {
        return uuid;
    }

    public void teleport(Vec3d pos) {
        removePath();
        this.pos = pos;
    }

    public void removePath() {
        setPath(null);
    }

    public void move(Vec3d pos, int ticks) {
        removePath();
        setPath(new LinePath(pos.subtract(this.pos), ticks));
    }

    public void setPath(@Nullable Path p) {
        pos = getPos();
        lastPathFinished = world.getGameTime();
        currentPath = p;
    }

    public void attachCallback(Callback callback) {
        this.callback = callback;
    }

    public void updateFrom(NBTTagCompound tag) {
        pos = new Vec3d(tag.getDouble("posX"), tag.getDouble("posY"), tag.getDouble("posZ"));
        size = new Vec3d(tag.getDouble("sizeX"), tag.getDouble("sizeY"), tag.getDouble("sizeZ"));
        if (tag.hasKey("callback")) {
            NBTTagCompound callbackTag = tag.getCompound("callback");
            callback = CallbackType.load(callbackTag);
        } else callback = null;
        if (currentPath != null) {
            NBTTagCompound pathTag = tag.getCompound("path");
            currentPath = PathType.load(pathTag);
        } else currentPath = null;
        lastPathFinished = tag.getLong("last_path");
    }

    public NBTTagCompound save() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setUniqueId("id", uuid);
        tag.setDouble("posX", pos.x);
        tag.setDouble("posY", pos.y);
        tag.setDouble("posZ", pos.z);
        tag.setDouble("sizeX", size.x);
        tag.setDouble("sizeY", size.y);
        tag.setDouble("sizeZ", size.z);
        if (callback != null) {
            NBTTagCompound callbackTag = new NBTTagCompound();
            callback.save(callbackTag);
            tag.setTag("callback", callbackTag);
        }
        if (currentPath != null) {
            NBTTagCompound pathTag = new NBTTagCompound();
            currentPath.save(pathTag);
            tag.setTag("path", pathTag);
        }
        tag.setLong("last_path", lastPathFinished);
        return tag;
    }

    public NBTTagCompound saveRef() {
        NBTTagCompound tag = new NBTTagCompound();
        return tag;
    }

    public static MovingStructure load(World world, NBTTagCompound tag) {
        UUID uuid = tag.getUniqueId("id");
        MovingStructure structure = new MovingStructure(world, uuid);
        structure.updateFrom(tag);
        return structure;
    }

}
