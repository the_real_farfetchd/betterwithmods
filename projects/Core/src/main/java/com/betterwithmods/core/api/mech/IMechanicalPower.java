/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.api.mech;

import com.betterwithmods.core.api.BWMApi;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface IMechanicalPower {


    /**
     * @param facing - side from which the power is being emitted
     * @return Returns IPower value of that side, can be null if that side does not provide any power
     */
    @Nullable
    IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull EnumFacing facing);

    /**
     * @param world  World of the TileEntity
     * @param pos    Position of the TileEntity
     * @param facing opposite of side from which it looks fro IMechanicalPower providers
     * @return Finds valid IMechanicalPower providers at the opposite of facing, full if no provider is found or {@link IMechanicalPower#getOutput(IWorld, BlockPos, EnumFacing)} returns null;
     */
    @Nullable
    default IPower getInput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull EnumFacing facing) {
        BlockPos offset = pos.offset(facing);
        EnumFacing outputFace = facing.getOpposite();
        LazyOptional<IMechanicalPower> optionalMech = BWMApi.MECHANICAL_UTILS.getMechanicalPower(world, offset, outputFace);
        if (optionalMech.isPresent()) {
            IMechanicalPower mech = optionalMech.orElse(null);
            return mech.getOutput(world, pos.offset(facing), outputFace);
        }
        return null;
    }

    /**
     * @param facing input direction with maximum
     * @return power value that will cause an overload if exceed. null for no maximum
     */
    @Nullable
    default IPower getMaximumInput(@Nonnull EnumFacing facing) {
        return null;
    }

    /**
     * @return power value that will be skipped if not exceed. null for no minimum.
     */
    @Nullable
    default IPower getMinimumInput() {
        return null;
    }

    /**
     * @param facing side from which the machine can get inputs
     * @return whether the machine can get input from this side.
     */
    boolean canInputFrom(EnumFacing facing);

    /**
     * @param world World of tileentity
     * @param pos   position of tileentity
     *              action to occur when receiving power from too many sources or from power that exceeds {@link IMechanicalPower#getMaximumInput(EnumFacing)}
     * @return if this method should stop power calculation
     */
    boolean overpower(World world, BlockPos pos);
}

