/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.relationary;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;

@SuppressWarnings("UnstableApiUsage")
public class Relation {

    private static final Logger logger = LogManager.getLogger("Relationships");
    protected static int VERSION;

    private ResourceLocation registryName;

    private final Multimap<Shape, ItemStack> shapeToItemstacks;
    private final Multimap<HashableItemStack, Shape> itemStackToShape;


    public Relation(ResourceLocation id) {
        this.registryName = id;
        shapeToItemstacks = MultimapBuilder.hashKeys().arrayListValues().build();
        itemStackToShape = MultimapBuilder.hashKeys().hashSetValues().build();
    }

    private static void registerEntry(Relation relation, JsonObject entry) {

        ResourceLocation shapeName = new ResourceLocation(entry.get("shape").getAsString());
        if (!RelationaryManager.shapes.containsKey(shapeName)) {
            throw new IllegalStateException("Unregistered shape used in relationship:" + shapeName);
        }

        List<ItemStack> stacks = Lists.newArrayList();
        JsonArray items = JsonUtils.getJsonArray(entry, "items");
        for (JsonElement i : items) {
            JsonObject item = i.getAsJsonObject();
            try {
                ItemStack stack = ShapedRecipe.deserializeItem(item);
                stacks.add(stack);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        Shape shape = RelationaryManager.shapes.get(shapeName);
        relation.register(shape, stacks);
    }

    public static void deserialize(Relation relation, JsonObject jsonobject) {
        JsonArray entries = jsonobject.get("relations").getAsJsonArray();
        for (JsonElement e : entries) {
            JsonObject entry = e.getAsJsonObject();
            registerEntry(relation, entry);
        }
    }

    public Collection<ItemStack> fromShape(Shape shape) {
        Collection<ItemStack> stacks = shapeToItemstacks.get(shape);
        return stacks.stream().map(ItemStack::copy).collect(ImmutableList.toImmutableList());
    }

    public ItemStack first(Shape shape, int count) {
        ItemStack first = fromShape(shape).stream().findFirst().orElse(ItemStack.EMPTY);
        first.setCount(count);
        return first;
    }

    public Collection<Shape> fromStack(ItemStack stack) {
        return itemStackToShape.get(new HashableItemStack(stack));
    }

    private void register(@Nonnull Shape shape, Collection<ItemStack> stacks) {
        this.shapeToItemstacks.putAll(shape, stacks);
        for (ItemStack stack : stacks) {
            HashableItemStack hashed = new HashableItemStack(stack);
            itemStackToShape.put(hashed, shape);
            RelationaryManager.ITEM_STACK_TO_RELATION.put(hashed, this);
        }

        logger.info("Adding Shape: {} with stacks: {}", shape, StringUtils.join(stacks, ","));
    }


    @Override
    public int hashCode() {
        return getRegistryName().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Relation) {
            return ((Relation) o).getRegistryName().equals(this.getRegistryName());
        }
        return false;
    }

    public ResourceLocation getRegistryName() {
        return registryName;
    }


    public static class Wrapper extends Relation {
        private int lastVersion = -1;

        private Relation cachedRelation;

        public Wrapper(ResourceLocation id) {
            super(id);
        }

        private void validateCache() {
            if (lastVersion != VERSION) {
                this.cachedRelation = RelationaryManager.getOrCreateRelation(getRegistryName());
                this.lastVersion = VERSION;
            }
        }

        @Override
        public Collection<ItemStack> fromShape(Shape shape) {
            validateCache();
            return cachedRelation.fromShape(shape);
        }

        @Override
        public Collection<Shape> fromStack(ItemStack stack) {
            validateCache();
            return cachedRelation.fromStack(stack);
        }
    }


}
