/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.client.gui;

import com.betterwithmods.core.base.game.container.BaseContainer;
import com.betterwithmods.core.base.game.tile.TileBase;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

import java.util.function.Supplier;

public class BaseContainerScreen<C extends BaseContainer, T extends TileBase> extends GuiContainer {

    protected C container;
    protected T tile;

    private final ResourceLocation background;

    public BaseContainerScreen(C container, T tile, ResourceLocation background) {
        super(container);
        this.container = container;
        this.tile = tile;
        this.background = background;
    }

    public void drawName(Supplier<ITextComponent> nameSupplier, int nameY, int playerX, int playerY) {
        String name = nameSupplier.get().getFormattedText();
        int nameWidth = this.fontRenderer.getStringWidth(name) / 2;
        this.fontRenderer.drawString(name, (float) (this.xSize / 2 - nameWidth), nameY, 0x404040);
        this.fontRenderer.drawString(container.player.inventory.getDisplayName().getFormattedText(), playerX, (float) (this.ySize - playerY), 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(background);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
    }

    public void setWidth(int w) {
        this.xSize = w;
    }

    public void setHeight(int h) {
        this.ySize = h;
    }

}
