/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.ai;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.tags.Tag;
import net.minecraft.util.math.AxisAlignedBB;

import java.util.List;

public class EntityAIPickupFeed extends EntityAIBase {

    private EntityLiving entity;
    private Tag<Item> feedTag;
    private int feedCooldownTime;
    protected int feedCooldown;
    private AxisAlignedBB pickupRange;

    public EntityAIPickupFeed(EntityCreature entity, Tag<Item> feedTag, int feedCooldownTime) {
        this.entity = entity;
        this.feedTag = feedTag;
        this.feedCooldownTime = feedCooldownTime;
        this.pickupRange = new AxisAlignedBB(entity.getPosition()).grow(1);
    }

    @Override
    public boolean shouldContinueExecuting() {
        return true;
    }

    @Override
    public boolean isInterruptible() {
        return true;
    }

    @Override
    public void startExecuting() {
        super.startExecuting();
    }

    @Override
    public void resetTask() {
        super.resetTask();
    }

    @Override
    public void tick() {
        super.tick();
        if(feedCooldown > 0) {
            feedCooldown--;
            return;
        }
        this.pickupRange = new AxisAlignedBB(entity.getPosition()).grow(1);

        List<EntityItem> foundItems = entity.getEntityWorld().getEntitiesWithinAABB(EntityItem.class, pickupRange);
        EntityItem foodToEat = foundItems.stream()
                .filter(entityItem -> feedTag.contains(entityItem.getItem().getItem()))
                .findFirst()
                .orElse(null);

        if(foodToEat != null) {
            if(entity instanceof EntityAnimal) {
                EntityAnimal animal = (EntityAnimal) entity;
                if (animal.isBreedingItem(foodToEat.getItem())) {
                    if (animal.getGrowingAge() == 0 && animal.canBreed()) {
                        animal.setInLove(null);
                    } else if (animal.isChild()) {
                        animal.ageUp((int) ((float) (-animal.getGrowingAge() / 20) * 0.1F), true);
                    }
                }
            }

            consumeFood(foodToEat);
        }

    }

    private void consumeFood(EntityItem food) {
        food.getItem().shrink(1);
        this.feedCooldown = this.feedCooldownTime;
        this.entity.playAmbientSound();
        this.entity.playSound(SoundEvents.ENTITY_GENERIC_EAT, 1, 1);
    }

    @Override
    public boolean shouldExecute() {
        return true;
    }
}
