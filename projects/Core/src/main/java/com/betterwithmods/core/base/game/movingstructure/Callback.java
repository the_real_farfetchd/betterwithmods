package com.betterwithmods.core.base.game.movingstructure;

import net.minecraft.nbt.NBTTagCompound;

public interface Callback {

    void onCompleted(MovingStructure structure);

    void onBlocked(MovingStructure structure);

    CallbackType type();

    /**
     * Call {@link CallbackType#save(Callback)} for saving
     */
    void save(NBTTagCompound nbt);

}
