/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.ingredient;

import com.betterwithmods.core.Core;
import com.betterwithmods.core.References;
import com.google.gson.JsonObject;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.common.crafting.IIngredientSerializer;

public class ToolIngredient extends PredicateIngredient {

    private ToolType toolType;

    public ToolIngredient(ToolType toolType) {
        super((stack) -> stack.getToolTypes().contains(toolType));
        this.toolType = toolType;
    }

    public static class Serializer implements IIngredientSerializer<ToolIngredient> {
        public static ResourceLocation NAME = new ResourceLocation(References.MODID_CORE, "tool_ingredient");

        @Override
        public ToolIngredient parse(PacketBuffer buffer) {
            String tooltype = buffer.readString(Short.MAX_VALUE);
            return new ToolIngredient(ToolType.get(tooltype));
        }

        @Override
        public ToolIngredient parse(JsonObject json) {
            String tooltype = JsonUtils.getString(json, "tooltype");
            return new ToolIngredient(ToolType.get(tooltype));
        }

        @Override
        public void write(PacketBuffer buffer, ToolIngredient ingredient) {
            buffer.writeString(ingredient.toolType.getName());
        }
    }

    @Override
    public IIngredientSerializer<? extends Ingredient> getSerializer() {
        return Core.TOOL_INGREDIENT;
    }

}
