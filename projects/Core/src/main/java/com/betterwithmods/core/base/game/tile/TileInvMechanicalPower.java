package com.betterwithmods.core.base.game.tile;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.function.Predicate;

public abstract class TileInvMechanicalPower extends TileMechanicalPower {

    @Nonnull
    private ItemStackHandler inventory = createInventory();

    protected Predicate<EnumFacing> inputs;

    public TileInvMechanicalPower(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
        inputs = f -> true;
    }

    @Nonnull
    public abstract ItemStackHandler createInventory();


    @Nonnull
    public ItemStackHandler getInventory() {
        return inventory;
    }


    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable EnumFacing side) {
        if (inputs.test(side) && cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.orEmpty(cap, LazyOptional.of(this::getInventory).cast());
        }
        return super.getCapability(cap, side);
    }


    @Override
    public NBTTagCompound write(NBTTagCompound compound) {
        compound.merge(inventory.serializeNBT());
        return super.write(compound);
    }

    @Override
    public void read(NBTTagCompound compound) {
        inventory.deserializeNBT(compound);
        super.read(compound);
    }

}
