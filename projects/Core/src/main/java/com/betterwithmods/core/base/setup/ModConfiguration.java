/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.setup;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.loading.FMLPaths;
import net.minecraftforge.fml.loading.FileUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.function.Function;

public class ModConfiguration<Client extends ConfigInstanceBase, Common extends ConfigInstanceBase>  {
    public ConfigInstance<Client> client;
    public ConfigInstance<Common> common;

    public ModConfiguration(Config<Client> client, Config<Common> common) {
        FileUtils.getOrCreateDirectory(FMLPaths.CONFIGDIR.get().resolve("betterwithmods"), "betterwithmods");
        this.common = new ConfigInstance<>(ModConfig.Type.COMMON, common);
        this.client = new ConfigInstance<>(ModConfig.Type.CLIENT, client);
    }


    @FunctionalInterface
    public interface Config<C extends ConfigInstanceBase> extends Function<ForgeConfigSpec.Builder, C> {
    }

    public class ConfigInstance<C extends ConfigInstanceBase> {
        private ForgeConfigSpec spec;
        private C config;

        public ConfigInstance(ModConfig.Type type, Config<C> configConstr) {
            Pair<C, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(configConstr);
            config = pair.getKey();
            spec = pair.getValue();
            String configName = String.format("%s-%s.toml", ModLoadingContext.get().getActiveContainer().getModId().replace("_", "/"), type.extension());
            ModLoadingContext.get().registerConfig(type, spec, configName);
        }

        public ForgeConfigSpec getSpec() {
            return spec;
        }

        public C getConfig() {
            return config;
        }
    }

    public Client client() {
        return client.getConfig();
    }

    public Common common() {
        return common.getConfig();
    }

}


