/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.potion.PotionEffect;

public class FoodProperties extends Item.Properties {

    protected int healAmount;
    protected float saturationAmount;
    protected boolean isMeat;
    protected boolean alwaysEdible;
    protected boolean fastEating;
    protected PotionEffect potionId;
    protected float potionEffectProbability;

    public FoodProperties healAmount(int healAmount) {
        this.healAmount = healAmount;
        return this;
    }

    public FoodProperties saturationAmount(float saturationAmount) {
        this.saturationAmount = saturationAmount;
        return this;
    }

    public FoodProperties isMeat(boolean isMeat) {
        this.isMeat = isMeat;
        return this;
    }

    public FoodProperties alwaysEdible(boolean alwaysEdible) {
        this.alwaysEdible = alwaysEdible;
        return this;
    }

    public FoodProperties fastEating(boolean fastEating) {
        this.fastEating = fastEating;
        return this;
    }

    public FoodProperties potionId(PotionEffect potionId) {
        this.potionId = potionId;
        return this;
    }

    public FoodProperties potionEffectProbability(float potionEffectProbability) {
        this.potionEffectProbability = potionEffectProbability;
        return this;
    }

    public FoodProperties group(ItemGroup group) {
        return (FoodProperties) super.group(group);
    }


}
