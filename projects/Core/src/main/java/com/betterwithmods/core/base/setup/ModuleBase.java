/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.setup;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import org.apache.logging.log4j.Logger;

public abstract class ModuleBase implements ISetup {

    private ForgeConfigSpec.BooleanValue enabled;

    private String name;

    private ModuleLoader loader;

    public ModuleBase(String name) {
        this.name = name;
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void onConfigure(CollectConfigEvent event) {
        if (loader == null) {
            getLogger().error("WTF THE LOADER IS MISSING");
            return;
        }


        if (!loader.modid.equals(event.getModid()))
            return;



        ForgeConfigSpec.Builder builder = event.getBuilder();
        if (event.getType() == ModConfig.Type.COMMON) {
            if (getDescription() != null)
                builder.comment(getDescription()).push(this.name);
            if (!isRequired()) {
                enabled = builder
                        .comment("Enable or disable this module")
                        .translation("config.betterwithmods_mechanist.module_enable")
                        .define("enabled", this::isEnabledByDefault);
            }
            configs(builder);
            builder.pop();
        }
    }

    public abstract void configs(ForgeConfigSpec.Builder builder);

    public boolean isEnabled() {
        return isRequired() || (enabled != null && enabled.get());
    }

    public boolean isRequired() {
        return false;
    }

    public void commonSetup(FMLCommonSetupEvent event) {

    }

    public void clientSetup(FMLClientSetupEvent event) {
    }

    public void loadComplete(FMLLoadCompleteEvent event) {
    }

    @Override
    public final void onCommonSetup(FMLCommonSetupEvent event) {
        if (isEnabled()) {
            getLogger().info("{}: {}", name, isEnabled() ? "Enabled" : "Disabled");
            commonSetup(event);
        }
    }

    @Override
    public final void onClientSetup(FMLClientSetupEvent event) {
        if (isEnabled()) {
            clientSetup(event);
        }
    }

    @Override
    public final void onLoadComplete(FMLLoadCompleteEvent event) {
        if (isEnabled()) {
            loadComplete(event);
        }
    }

    public void setLoader(ModuleLoader loader) {
        this.loader = loader;
    }

    public ModuleLoader getLoader() {
        return loader;
    }

    public Logger getLogger() {
        assert loader != null;
        return this.loader.getLogger();
    }

    public abstract String getDescription();

    public boolean isEnabledByDefault() {
        return true;
    }

    public String getName() {
        return name;
    }
}
