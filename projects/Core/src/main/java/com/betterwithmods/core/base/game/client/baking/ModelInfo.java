/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.client.baking;

import net.minecraft.block.state.IBlockState;
import net.minecraftforge.client.model.data.IModelData;

import java.util.function.Function;

public class ModelInfo<T> {

    private IBlockState state;
    private Function<IModelData, T> supplier;

    private boolean isItem;

    public ModelInfo(IBlockState state, Function<IModelData, T> supplier, boolean isItem) {
        this.state = state;
        this.supplier = supplier;
        this.isItem = isItem;
    }

    public IBlockState getState() {
        return state;
    }

    public Function<IModelData, T> getSupplier() {
        return supplier;
    }

    public boolean isItem() {
        return isItem;
    }

    public T apply(IModelData data) {
        return supplier.apply(data);
    }

}
