/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core;

import com.betterwithmods.core.base.game.ingredient.ToolIngredient;
import com.betterwithmods.core.base.game.recipes.ToolRecipe;
import com.betterwithmods.core.base.setup.ModBase;
import com.betterwithmods.core.base.setup.ModConfiguration;
import com.betterwithmods.core.config.CoreClient;
import com.betterwithmods.core.config.CoreCommon;
import com.betterwithmods.core.impl.CapabilityMechanicalPower;
import com.betterwithmods.core.relationary.RelationaryManager;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.RecipeSerializers;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.IIngredientSerializer;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerAboutToStartEvent;

@Mod(References.MODID_CORE)
public class Core extends ModBase {

    public static IIngredientSerializer<ToolIngredient> TOOL_INGREDIENT = CraftingHelper.register(ToolIngredient.Serializer.NAME, new ToolIngredient.Serializer());

    public static IRecipeSerializer<ToolRecipe> TOOL_RECIPE = RecipeSerializers.register(new ToolRecipe.Serializer());

    public Core() {
        super(References.MODID_CORE, References.NAME_CORE);
        configs = new ModConfiguration<>(CoreClient::new, CoreCommon::new);
        MinecraftForge.EVENT_BUS.register(this);

    }

    public static ModConfiguration<CoreClient, CoreCommon> configs;

    @Override
    public void onCommonSetup(FMLCommonSetupEvent event) {
        super.onCommonSetup(event);
        CapabilityMechanicalPower.register();
    }

    @SubscribeEvent
    public void serverAboutToStart(FMLServerAboutToStartEvent event) {
        MinecraftServer server = event.getServer();
        server.getResourceManager().addReloadListener(new RelationaryManager(getLogger()));
    }

}
