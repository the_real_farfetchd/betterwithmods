package com.betterwithmods.core.base.game.movingstructure;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;

public class LinePath implements Path {

    private final Vec3d offset;
    private final int maxTime;

    public LinePath(Vec3d offset, int maxTime) {
        this.offset = offset;
        this.maxTime = maxTime;
    }

    @Override
    public Vec3d getOffset(int ticks) {
        return offset.scale((double) ticks / maxTime);
    }

    @Override
    public int maxTime() {
        return maxTime;
    }

    @Override
    public PathType type() {
        return PathType.Line;
    }

    @Override
    public void save(NBTTagCompound nbt) {
        nbt.setDouble("x", offset.x);
        nbt.setDouble("y", offset.y);
        nbt.setDouble("z", offset.z);
        nbt.setInt("max_time", maxTime);
    }

    public static LinePath load(NBTTagCompound nbt) {
        Vec3d offset = new Vec3d(nbt.getDouble("x"), nbt.getDouble("y"), nbt.getDouble("z"));
        int maxTime = nbt.getInt("max_time");
        return new LinePath(offset, maxTime);
    }

}
