/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.ai;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;


public class AIFleeFromTarget extends EntityAIBase {

    private EntityLivingBase entity;

    private double maximumFleeDistance;
    private int maximumFleeDuration;

    private int fleeDuration;

    public AIFleeFromTarget(EntityLivingBase entity, double maximumFleeDistance, int maximumFleeDuration) {
        this.entity = entity;
        this.maximumFleeDistance = maximumFleeDistance;
        this.maximumFleeDuration = maximumFleeDuration;
    }

    @Override
    public boolean shouldContinueExecuting() {
        return entity.getRevengeTarget() != null && (fleeDuration < maximumFleeDuration || entity.getDistance(entity.getRevengeTarget()) <= maximumFleeDistance);
    }

    @Override
    public boolean isInterruptible() {
        return false;
    }

    @Override
    public void startExecuting() {
        super.startExecuting();
    }

    @Override
    public void resetTask() {
        super.resetTask();
    }

    @Override
    public boolean shouldExecute() {
        return entity.getRevengeTarget() != null;
    }

    @Override
    public void tick() {
        super.tick();
        this.fleeDuration++;
    }
}
