/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core;

import com.google.common.collect.ImmutableList;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.Tag;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class Tags {

    public static class Blocks {
        public static final Tag<Block> WOOD = tag(References.MODID_CORE, "wood");
        public static final Tag<Block> LOGS = tag(References.MODID_CORE, "logs");
        public static final Tag<Block> HANDCRANKABLE = tag(References.MODID_MECHANIST, "handcrankable");

        public static final Tag<Block> BARK_LOGS = tag(References.MODID_CORE, "bark_logs");

        public static Tag<Block> tag(String modid, String name) {
            return tag(new ResourceLocation(modid, name));
        }

        public static Tag<Block> tag(ResourceLocation tagLocation) {
            return new BlockTags.Wrapper(tagLocation);
        }
    }

    public static class Items {
        public static final Tag<Item> FIRESTARTERS = tag(References.MODID_CORE, "tools/firestarters");
        public static final Tag<Item> ADHESIVE = tag(References.MODID_MECHANIST, "adhesive");
        public static final Tag<Item> SOAP = tag(References.MODID_MECHANIST, "soap");

        public static final Tag<Item> DUST_WOOD = tag(References.FORGE, "dusts/wood");
        public static final Tag<Item> DUST_COAL = tag(References.FORGE, "dusts/coal");
        public static final Tag<Item> DUST_SULFUR = tag(References.FORGE, "dusts/sulfur");

        public static final Tag<Item> TIPPING_BLACKLIST = tag(References.MODID_MECHANIST, "tipping_blacklist");

        public static Tag<Item> tag(String modid, String name) {
            return tag(new ResourceLocation(modid, name));
        }

        public static Tag<Item> tag(ResourceLocation tagLocation) {
            return new ItemTags.Wrapper(tagLocation);
        }
    }


    public static <T extends IItemProvider> Optional<ItemStack> getItemStack(Tag<T> source, int count, String namespace) {
        return getItemStack(source, count, Comparator.comparing(o -> o.asItem().getRegistryName().getNamespace().equals(namespace)));
    }

    public static <T extends IItemProvider> Optional<ItemStack> getItemStack(Tag<T> source, int count, Comparator<T> comparator) {
        List<T> items = ImmutableList.sortedCopyOf(comparator, source.getAllElements());
        return items.stream().findFirst().map(i -> new ItemStack(i, count));
    }
}


