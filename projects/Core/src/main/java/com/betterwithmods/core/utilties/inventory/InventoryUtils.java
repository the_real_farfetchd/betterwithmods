/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties.inventory;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

import java.util.Iterator;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class InventoryUtils {

    public static Optional<ItemStack> findInInventory(IItemHandler handler, Ingredient ingredient, boolean extract) {
        return findInInventory(handler, ingredient, 64, extract);
    }

    public static Optional<ItemStack> findInInventory(IItemHandler handler, Ingredient ingredient, int count, boolean extract) {
        return stream(handler).map(slot -> {
            ItemStack stack = slot.getStack();
            if (extract) {
                slot.extract(count);
            }
            return stack;
        }).filter(ingredient).findFirst();
    }

    public static Iterable<ItemSlot> iterable(IItemHandler handler) {
        return new ItemHandlerIterable(handler);
    }

    public static Iterator<ItemSlot> iterator(IItemHandler handler) {
        return new ItemHandlerIterator(handler);
    }

    public static Stream<ItemSlot> stream(IItemHandler handler) {
        return StreamSupport.stream(iterable(handler).spliterator(), false);
    }

    public static IItemHandler getInventory(ICapabilityProvider provider, EnumFacing dir) {
        return provider.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, dir).orElse(null);
    }

    public static ItemStack addToInventory(IItemHandler handler, ItemStack stack) {
        for (ItemSlot slot : iterable(handler)) {
            ItemStack returned = handler.insertItem(slot.getSlot(), stack, false);
            if (returned.isEmpty() || (returned.getItem() == stack.getItem() && returned.getCount() != stack.getCount())) {
                return returned;
            }
        }
        return ItemStack.EMPTY;
    }

}
