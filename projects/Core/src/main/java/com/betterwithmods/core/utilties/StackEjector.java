/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import com.google.common.base.Preconditions;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import javax.annotation.Nonnull;

public class StackEjector {

    private ItemStack stack;
    private VectorBuilder positionBuilder, motionBuilder;
    private int pickupDelay;


    public StackEjector(VectorBuilder position, VectorBuilder motion) {
        this.positionBuilder = position;
        this.motionBuilder = motion;
    }

    public StackEjector setStack(@Nonnull ItemStack stack) {
        this.stack = stack;
        return this;
    }

    public VectorBuilder getPositionBuilder() {
        return positionBuilder;
    }

    public VectorBuilder getMotionBuilder() {
        return motionBuilder;
    }


    public void ejectStack(@Nonnull World world, @Nonnull Vec3d p, @Nonnull Vec3d m) {
        Preconditions.checkNotNull(stack, "stack");

        if (world.isRemote)
            return;
        Vec3d position = getPositionBuilder().build(p);
        Vec3d motion = getMotionBuilder().build(m);
        if (position == null)
            return;
        EntityItem item = new EntityItem(world, position.x, position.y, position.z, stack);
        item.setPickupDelay(pickupDelay);
        if (motion != null) {
            item.motionX = motion.x;
            item.motionY = motion.y;
            item.motionZ = motion.z;
        }
        world.spawnEntity(item);
    }

    public void ejectStack(@Nonnull World world, @Nonnull BlockPos p, @Nonnull BlockPos m) {
        ejectStack(world, new Vec3d(p), new Vec3d(m));
    }

    public StackEjector setPickupDelay(int pickupDelay) {
        this.pickupDelay = pickupDelay;
        return this;
    }


}
