/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.vitality.modules;

import com.betterwithmods.core.base.setup.ModuleBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class Insomnia extends ModuleBase {

    private ForgeConfigSpec.BooleanValue stillSetSpawn;

    public Insomnia() {
        super("insomnia");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        this.stillSetSpawn = builder.comment("Beds will still set the spawn of the player")
                .translation("config.betterwithmods_vitality.bedsStillSetSpawn")
                .define("bedsStillSetSpawn", false);

    }

    @SubscribeEvent
    public void onSleepInBed(PlayerSleepInBedEvent event) {
        if(!event.getEntityPlayer().isCreative()) {
            event.getEntityPlayer().sendStatusMessage(new TextComponentTranslation("betterwithmods_vitality:tile.bed.tooRestless"), true);
            event.setResult(EntityPlayer.SleepResult.OTHER_PROBLEM);
        }
    }

    @Override
    public String getDescription() {
        return "Players are no longer have to sleep in any bed. Prevents skipping the night.";
    }
}
