/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.vitality.modules;

import com.betterwithmods.core.References;
import com.betterwithmods.core.Tags;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.google.common.collect.Maps;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.tags.Tag;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

import java.util.Map;
import java.util.UUID;

public class Movement extends ModuleBase {

    private static final UUID MOVEMENT_SPEED_UUID = UUID.fromString("91AEAA56-376B-4498-935B-2F7F68070635");
    private Map<Material, MovementModifier> MATERIAL_MOVEMENT_MODIFIERS = Maps.newHashMap();

    public Movement() {
        super("movement");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {

    }

    @Override
    public void commonSetup(FMLCommonSetupEvent event) {
        super.commonSetup(event);

        MATERIAL_MOVEMENT_MODIFIERS.put(Material.ROCK, MovementModifier.FAST);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.ROCK, MovementModifier.FAST);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.WOOD, MovementModifier.FAST);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.IRON, MovementModifier.FAST);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.CLOTH, MovementModifier.FAST);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.CARPET, MovementModifier.FAST);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.CIRCUITS, MovementModifier.FAST);

        MATERIAL_MOVEMENT_MODIFIERS.put(Material.GRASS, MovementModifier.SLOW);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.GLASS, MovementModifier.SLOW);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.GROUND, MovementModifier.SLOW);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.CLAY, MovementModifier.SLOW);

        MATERIAL_MOVEMENT_MODIFIERS.put(Material.SAND, MovementModifier.VERY_SLOW);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.SNOW, MovementModifier.VERY_SLOW);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.LEAVES, MovementModifier.VERY_SLOW);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.PLANTS, MovementModifier.VERY_SLOW);
        MATERIAL_MOVEMENT_MODIFIERS.put(Material.VINE, MovementModifier.VERY_SLOW);

    }

    @SubscribeEvent
    public void onWalk(TickEvent.PlayerTickEvent event) {
        if(event.phase != TickEvent.Phase.END || event.player.isCreative()) return;
        EntityPlayer player = event.player;
        IAttributeInstance movementSpeedAttribute = player.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED);
        AttributeModifier movementSpeedModifier = MovementModifier.NORMAL.getAttributeModifier();

        if(player.onGround) {
            BlockPos posBelowPlayer = player.getPosition().add(0.0, 0.5D, 0.0);
            IBlockState state = player.getEntityWorld().getBlockState(posBelowPlayer);

            if (MATERIAL_MOVEMENT_MODIFIERS.containsKey(state.getMaterial())) {
                movementSpeedModifier = MATERIAL_MOVEMENT_MODIFIERS.get(state.getMaterial()).getAttributeModifier();
            }

            for(MovementModifier modifier : MovementModifier.values()) {
                Tag<Block> modifierTag = modifier.getTag();
                if(state.getBlock().isIn(modifierTag)) {
                    movementSpeedModifier = modifier.getAttributeModifier();
                }
            }
        }

        if(movementSpeedAttribute.hasModifier(movementSpeedModifier)) {
            movementSpeedAttribute.removeModifier(movementSpeedModifier);
        }

        movementSpeedAttribute.applyModifier(movementSpeedModifier);

    }

    @SubscribeEvent
    public void onFOV(FOVUpdateEvent event) {
        EntityPlayer player = event.getEntity();
        float f = 1.0F;

        if (player.abilities.isFlying) {
            f *= 1.1F;
        }

        IAttributeInstance movementSpeedAttribute = player.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED);

        double value = movementSpeedAttribute.getValue();
        AttributeModifier mod = movementSpeedAttribute.getModifier(MOVEMENT_SPEED_UUID);
        if (mod != null)
            value /= (1 + mod.getAmount());
        f = (float) ((double) f * ((value / (double) player.abilities.getWalkSpeed() + 1.0D) / 2.0D));

        if (player.abilities.getWalkSpeed() == 0.0F || Float.isNaN(f) || Float.isInfinite(f)) {
            f = 1.0F;
        }
        if (player.isHandActive() && player.getActiveItemStack().getItem() == Items.BOW) {
            int i = player.getItemInUseMaxCount();
            float f1 = (float) i / 20.0F;

            if (f1 > 1.0F) {
                f1 = 1.0F;
            } else {
                f1 = f1 * f1;
            }

            f *= 1.0F - f1 * 0.15F;
        }

        event.setNewfov(f);
    }


    @Override
    public String getDescription() {
        return "Player movement depends on the block they are walking on";
    }

    private enum MovementModifier {
        VERY_FAST(1.5F, "very_fast"),
        FAST(1.25F, "fast"),
        NORMAL(1.0F, "normal"),
        SLOW(0.75F, "slow"),
        VERY_SLOW(0.5f, "very_slow");

        private float modifier;
        private Tag<Block> tag;
        MovementModifier(float modifier, String tagName) {
            this.modifier = modifier;
            this.tag = Tags.Blocks.tag(References.MODID_VITALITY, "movement/" + tagName);
        }

        public AttributeModifier getAttributeModifier() {
            return new AttributeModifier(MOVEMENT_SPEED_UUID, "movement", modifier - 1, 2);
        }

        public Tag<Block> getTag() {
            return tag;
        }

    }


}
