/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.redstone;

import net.minecraft.block.BlockObserver;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public class BlockAdvancedObserver extends BlockObserver {

    public BlockAdvancedObserver(Properties builder) {
        super(builder);
    }

    @Override
    public IBlockState updatePostPlacement(IBlockState stateIn, EnumFacing facing, IBlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        if (stateIn.get(FACING) != facing && !stateIn.get(POWERED)) {
            this.startSignal(worldIn, currentPos);
        }
        return stateIn;
    }

    private void startSignal(IWorld world, BlockPos pos) {
        if (!world.isRemote() && !world.getPendingBlockTicks().isTickScheduled(pos, this)) {
            world.getPendingBlockTicks().scheduleTick(pos, this, 2);
        }
    }

    @Override
    protected void updateNeighborsInFront(World worldIn, BlockPos pos, IBlockState state) {
        EnumFacing front = state.get(FACING);
        for (EnumFacing facing : EnumFacing.values()) {
            if (facing != front) {
                BlockPos blockpos = pos.offset(facing.getOpposite());
                worldIn.neighborChanged(blockpos, this, pos);
                worldIn.notifyNeighborsOfStateExcept(blockpos, this, front);
            }
        }
    }


    @Override
    public int getWeakPower(IBlockState state, IBlockReader blockAccess, BlockPos pos, EnumFacing side) {
        return state.get(POWERED) && state.get(FACING) == side.getOpposite() ? 15 : 0;
    }

    @Override
    public IBlockState getStateForPlacement(BlockItemUseContext context) {
        EnumFacing facing = context.getNearestLookingDirection().getOpposite();
        return this.getDefaultState().with(FACING, facing);
    }

}
