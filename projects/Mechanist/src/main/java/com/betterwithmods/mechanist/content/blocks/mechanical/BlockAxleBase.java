/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.mechanical;

import com.betterwithmods.core.base.game.block.BlockBaseWaterlogged;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.mechanist.content.blocks.mechanical.generator.BlockAxleGenerator;
import net.minecraft.block.Block;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.init.Blocks;
import net.minecraft.init.Particles;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

public class BlockAxleBase extends BlockBaseWaterlogged {

    public static final EnumProperty<EnumFacing.Axis> AXIS = BlockStateProperties.AXIS;
    public static final BooleanProperty POWERED = BlockStateProperties.POWERED;

    public BlockAxleBase(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(AXIS, EnumFacing.Axis.Y).with(POWERED, false));
        setIsFullCube(state -> false);
        setRenderLayer(BlockRenderLayer.CUTOUT);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(AXIS,POWERED);
    }

    @Nullable
    @Override
    public IBlockState getStateForPlacement(BlockItemUseContext context) {
        return getDefaultState()
                .with(AXIS, context.getFace().getAxis())
                .with(WATERLOGGED, shouldWaterlog(context));
    }


    @Override
    public BlockFaceShape getBlockFaceShape(IBlockReader worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        return SHAPES[state.get(AXIS).ordinal()];
    }

    private static final VoxelShape[] SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(0, 6, 6, 16, 10, 10),
            Block.makeCuboidShape(6, 0, 6, 10, 16, 10),
            Block.makeCuboidShape(6, 6, 0, 10, 10, 16)
    };

    public EnumFacing.Axis getAxis(IBlockState state) {
        return state.get(AXIS);
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    @Override
    public void animateTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        if(stateIn.get(POWERED)) {
            EnvironmentUtils.forEachParticle(worldIn, pos, Particles.SMOKE, 3, EnvironmentUtils.RANDOM_PARTICLE, EnvironmentUtils.SPEED);
        }
    }

    @Override
    public boolean isReplaceable(IBlockState state, BlockItemUseContext useContext) {
        ItemStack itemstack = useContext.getItem();
        if (ItemUtils.getBlock(itemstack).orElse(Blocks.AIR) instanceof BlockAxleGenerator) {
            return useContext.replacingClickedOnBlock();
        }
        return false;
    }
}
