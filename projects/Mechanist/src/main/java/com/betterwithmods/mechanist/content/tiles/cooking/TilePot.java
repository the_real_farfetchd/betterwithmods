/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.tiles.cooking;

import com.betterwithmods.core.Tags;
import com.betterwithmods.core.api.mech.IMechanicalPower;
import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.ingredient.PredicateIngredient;
import com.betterwithmods.core.base.game.tile.TileInventory;
import com.betterwithmods.core.impl.CapabilityMechanicalPower;
import com.betterwithmods.core.impl.Power;
import com.betterwithmods.core.utilties.FacingUtils;
import com.betterwithmods.core.utilties.MechanicalUtils;
import com.betterwithmods.core.utilties.StackEjector;
import com.betterwithmods.core.utilties.VectorBuilder;
import com.betterwithmods.core.utilties.inventory.InventoryUtils;
import com.betterwithmods.mechanist.content.blocks.cooking.BlockPot;
import com.betterwithmods.mechanist.content.container.PotContainer;
import com.google.common.collect.Lists;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IInteractionObject;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

public abstract class TilePot extends TileInventory implements IMechanicalPower, IInteractionObject, ITickable {

    private EnumFacing tilt;

    private byte stage;
    private int delay;

    public TilePot(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }

    @Nonnull
    @Override
    public ItemStackHandler createInventory() {
        return new ItemStackHandler(27);
    }

    @Override
    public void tick() {
        if (tilt.getAxis().isVertical())
            return;
        if (delay <= 0) {
            switch (stage) {
                case 1: {
                    delay = 20 * 2;
                    stage = 2;
                    break;
                }
                case 2: {
                    delay = 20;
                    spillContents();
                    break;
                }
            }
        }
        delay = Math.max(0, delay - 1);
    }


    public void spillContents() {
        VectorBuilder pos = new VectorBuilder().offset(tilt, 0.75).offset(0.5, 0.25, 0.5);
        VectorBuilder motion = new VectorBuilder().offset(tilt, 0.075);
        StackEjector ejector = new StackEjector(pos, motion);

        Ingredient notEmpty = (Ingredient) new PredicateIngredient(ItemStack::isEmpty).negate();
        Ingredient notBlacklisted = (Ingredient) Ingredient.fromTag(Tags.Items.TIPPING_BLACKLIST).negate();

        Optional<ItemStack> optionalItemStack = InventoryUtils.findInInventory(getInventory(), Ingredient.merge(Lists.newArrayList(notEmpty, notBlacklisted)), 8, true);
        optionalItemStack.ifPresent(stack ->
                ejector.setStack(stack).ejectStack(world, getPos(), new BlockPos(0, 0, 0)));
    }


    @Override
    public void onChanged() {
        markDirty();

        int sources = 0;
        EnumFacing side = null;
        for (EnumFacing facing : FacingUtils.HORIZONTAL_SET) {
            IPower input = getInput(world, pos, facing);

            if (input != null) {

                int torque = input.getTorque();
                IPower minInput = getMinimumInput();
                if (minInput != null && torque < minInput.getTorque()) {
                    continue;
                }

                if (sources > 0) {
                    MechanicalUtils.get(world, pos.offset(facing), facing.getOpposite()).ifPresent(m -> m.overpower(world, pos.offset(facing)));
                    continue;
                }
                side = facing;

                sources++;
            }
        }
        if (side != null) {
            tilt = side.rotateY();
            stage = 1;
        } else {
            tilt = EnumFacing.UP;
        }

        markDirty();
        if (getBlockState().getBlock() instanceof BlockPot) {
            BlockPot pot = (BlockPot) getBlockState().getBlock();
            pot.tilt(world, pos, tilt);
        }
    }


    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull EnumFacing facing) {
        return null;
    }

    @Nullable
    @Override
    public IPower getMaximumInput(@Nonnull EnumFacing facing) {
        return null;
    }

    @Nullable
    @Override
    public IPower getMinimumInput() {
        return Power.ONE_TORQUE;
    }

    @Override
    public boolean canInputFrom(EnumFacing facing) {
        return facing.getAxis().isHorizontal();
    }

    @Override
    public boolean overpower(World world, BlockPos pos) {
        return false;
    }

    private static final String TILT = "tilt", STAGE = "stage", DELAY = "delay";

    @Nonnull
    @Override
    public NBTTagCompound write(NBTTagCompound compound) {
        FacingUtils.write(compound, TILT, tilt);
        compound.setByte(STAGE, stage);
        compound.setInt(DELAY, delay);
        return super.write(compound);
    }

    @Override
    public void read(NBTTagCompound compound) {
        super.read(compound);
        tilt = FacingUtils.read(compound, TILT);
        if (compound.hasKey(STAGE)) {
            stage = compound.getByte(STAGE);
        }
        if (compound.hasKey(DELAY)) {
            delay = compound.getByte(DELAY);
        }
    }

    @Nonnull
    @Override
    public Container createContainer(@Nonnull InventoryPlayer playerInventory, @Nonnull EntityPlayer playerIn) {
        return new PotContainer(playerIn, this);
    }

    @Nonnull
    @Override
    public String getGuiID() {
        return getType().getRegistryName().toString();
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    @Nullable
    @Override
    public ITextComponent getCustomName() {
        return null;
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable EnumFacing side) {
        if (cap == CapabilityMechanicalPower.MECHANICAL_POWER) {
            return CapabilityMechanicalPower.MECHANICAL_POWER.orEmpty(cap, LazyOptional.of(() -> this));
        }
        return super.getCapability(cap, side);
    }

}
