/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.tiles.mechanical;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.TileInvMechanicalPower;
import com.betterwithmods.core.utilties.FacingUtils;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.container.PulleyContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.IInteractionObject;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;

public class TilePulley extends TileInvMechanicalPower implements IInteractionObject, ITickable {

    public TilePulley() {
        super(Registrars.Tiles.TILE_PULLEY);
    }

    @Override
    public void tick() {
    }

    @Override
    protected boolean isMechanicalSide(EnumFacing facing) {
        return facing.getAxis() != EnumFacing.Axis.Y;
    }

    @Nonnull
    @Override
    public Iterable<EnumFacing> getInputs() {
        return FacingUtils.HORIZONTAL_SET;
    }

    @Nonnull
    @Override
    public Iterable<EnumFacing> getOutputs() {
        return Collections.emptySet();
    }

    @Override
    public void updateBlock() {

    }

    @Nonnull
    @Override
    public ItemStackHandler createInventory() {
        return new ItemStackHandler(4);
    }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull EnumFacing facing) {
        return null;
    }

    @Override
    public boolean overpower(World world, BlockPos pos) {
        return false;
    }

    @Override
    public Container createContainer(InventoryPlayer inventoryPlayer, EntityPlayer entityPlayer) {
        return new PulleyContainer(entityPlayer, this);
    }

    @Override
    public String getGuiID() {
        return getType().getRegistryName().toString();
    }

    @Override
    public ITextComponent getName() {
        return new TextComponentTranslation(String.format("container.%s.pulley", References.MODID_MECHANIST));
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    @Nullable
    @Override
    public ITextComponent getCustomName() {
        return null;
    }
}
