/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks;

import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.mechanist.content.tiles.TileCamoflage;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.stats.StatList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public abstract class BlockCamo extends BlockBase {
    public BlockCamo(Properties properties) {
        super(properties);
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(IBlockState state, IBlockReader world) {
        return new TileCamoflage();
    }

    public boolean hasTileEntity(IBlockState state) {
        return true;
    }

    @Override
    public abstract void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items);

    @Override
    public ItemStack getPickBlock(IBlockState state, @Nullable RayTraceResult target, IBlockReader world, BlockPos pos, EntityPlayer player) {
        TileEntity tile = world.getTileEntity(pos);
        return tile instanceof TileCamoflage ? ((TileCamoflage) tile).getItemStack(state) : ItemStack.EMPTY;
    }

    @Override
    public ITextComponent getNameTextComponent() {
        return super.getNameTextComponent();
    }

    public IBlockState getParent(ItemStack stack) {
        NBTTagCompound tag = stack.getChildTag("BlockEntityTag");
        if (tag != null && tag.hasKey("parent")) {
            return NBTUtil.readBlockState(tag.getCompound("parent"));
        }
        return null;
    }

    @Override
    public void harvestBlock(World worldIn, EntityPlayer player, BlockPos pos, IBlockState state, @Nullable TileEntity te, ItemStack stack) {
        if (te instanceof TileCamoflage) {
            spawnAsEntity(worldIn, pos, ((TileCamoflage) te).getItemStack(state));
            player.addStat(StatList.BLOCK_MINED.get(this));
        } else {
            super.harvestBlock(worldIn, player, pos, state, null, stack);
        }
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, @Nullable EntityLivingBase placer, ItemStack stack) {
        TileEntity tile = worldIn.getTileEntity(pos);
        if (tile instanceof TileCamoflage) {
            ((TileCamoflage) tile).loadFromItemStack(stack);
        }
    }
}
