/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.tiles.mechanical.generator;

import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.TileGenerator;
import com.betterwithmods.core.impl.Power;
import com.betterwithmods.core.utilties.FacingUtils;
import com.betterwithmods.mechanist.content.blocks.mechanical.BlockAxleBase;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public abstract class TileAxleGenerator extends TileGenerator {

    public Power powerSource = null;

    public TileAxleGenerator(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }

    private EnumFacing.Axis getAxis() {
        IBlockState state = world.getBlockState(pos);
        if(state.getBlock() instanceof BlockAxleBase)
            return state.get(BlockAxleBase.AXIS);
        return null;
    }

    private boolean onAxis(EnumFacing facing) {
        return facing != null && facing.getAxis().equals(getAxis());
    }

    @Nonnull
    @Override
    public Iterable<EnumFacing> getOutputs() {
        return FacingUtils.fromAxis(getAxis());
    }


    @Override
    public void updateBlock() {
        IBlockState state = world.getBlockState(pos);
        if(state.getBlock() instanceof BlockAxleBase) {
            world.setBlockState(pos, state.with(BlockAxleBase.POWERED, power.getTorque() > 0));
        }
    }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull EnumFacing facing) {
        if(onAxis(facing)) {
            return super.getOutput(world,pos, facing);
        }
        return null;
    }


}
