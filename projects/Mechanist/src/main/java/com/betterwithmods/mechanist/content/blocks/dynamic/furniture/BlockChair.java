/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.dynamic.furniture;

import com.betterwithmods.core.utilties.VoxelUtils;
import com.betterwithmods.mechanist.content.blocks.dynamic.BlockDynWaterRotate;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

//TODO Make sittable
public class BlockChair extends BlockDynWaterRotate {

    private static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    public BlockChair(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(FACING, EnumFacing.SOUTH));
        setIsFullCube(state -> false);
        setRenderLayer(BlockRenderLayer.CUTOUT);
    }

    @Override
    public void onRotate(World worldIn, BlockPos pos, IBlockState state) {
        worldIn.setBlockState(pos, state.cycle(FACING));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(FACING);
    }

    @Nullable
    @Override
    public IBlockState getStateForPlacement(BlockItemUseContext context) {
        return getDefaultState().with(FACING, context.getPlacementHorizontalFacing());
    }

    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        return SHAPES[state.get(FACING).getHorizontalIndex()];
    }

    private final static VoxelShape[] SHAPES;

    static {
        VoxelShape
                leg1 = Block.makeCuboidShape(2, 0, 12, 4, 6, 14),
                leg2 = Block.makeCuboidShape(12, 0, 2, 14, 6, 4),
                leg3 = Block.makeCuboidShape(12, 0, 12, 14, 6, 14),
                leg4 = Block.makeCuboidShape(2, 0, 2, 4, 6, 4);
        VoxelShape bottom = Block.makeCuboidShape(2, 6, 2, 14, 8, 14);
        VoxelShape chair_bottom = VoxelUtils.or(bottom, leg1, leg2, leg3, leg4);


        SHAPES = new VoxelShape[]{
                VoxelUtils.or(chair_bottom, Block.makeCuboidShape(2, 8, 2, 14, 16, 4)), //south
                VoxelUtils.or(chair_bottom, Block.makeCuboidShape(12, 8, 2, 14, 16, 14)), //east
                VoxelUtils.or(chair_bottom, Block.makeCuboidShape(2, 8, 12, 14, 16, 14)), //north
                VoxelUtils.or(chair_bottom, Block.makeCuboidShape(2, 8, 2, 4, 16, 14)), //west
        };

    }

    @Override
    public void addInformation(ItemStack stack, @Nullable IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new TextComponentString("WIP - Cannot sit yet"));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}
