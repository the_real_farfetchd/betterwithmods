/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.items;

import com.betterwithmods.core.base.game.ConstantIngredients;
import com.betterwithmods.core.base.game.item.ItemBase;
import com.betterwithmods.core.utilties.PlayerUtils;
import com.betterwithmods.mechanist.content.entities.EntityDynamite;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;

import java.util.Optional;

public class ItemDynamite extends ItemBase {
    public ItemDynamite(Properties properties) {
        super(properties);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);
        if (!player.isCreative()) {
            stack.shrink(1);
        }

        world.playSound(null, player.posX, player.posY, player.posZ, SoundEvents.ENTITY_SNOWBALL_THROW, SoundCategory.NEUTRAL, 0.5F, 0.4F / (random.nextFloat() * 0.4F + 0.8F));
        if (!world.isRemote) {
            EntityDynamite entity = new EntityDynamite(world, player, calculateFuse(player));
            world.spawnEntity(entity);
        }

        player.addStat(StatList.ITEM_USED.get(this));
        return new ActionResult<>(EnumActionResult.PASS, stack);
    }


    public int calculateFuse(EntityPlayer player) {
        Optional<ItemStack> firestarters = PlayerUtils.findInPlayer(player, ConstantIngredients.FIRESTARTER, false);
        if(firestarters.isPresent()) {
            ItemStack firestarter = firestarters.get();
            if(firestarter.isDamageable()) {
                firestarter.damageItem(1, player);
            } else {
                firestarter.shrink(1);
            }
            return 80;
        }
        return -1;
    }
}
