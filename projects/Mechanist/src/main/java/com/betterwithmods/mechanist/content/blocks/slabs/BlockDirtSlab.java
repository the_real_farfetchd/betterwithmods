/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.slabs;

import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReaderBase;
import net.minecraft.world.World;

import java.util.Random;

public class BlockDirtSlab extends BlockDirtSnowySlab {
    public BlockDirtSlab(Properties builder) {
        super(builder);
    }


    @Override
    public void tick(IBlockState state, World worldIn, BlockPos pos, Random random) {
        super.tick(state,worldIn,pos,random);

        if (!worldIn.isRemote) {
            if (worldIn.getLight(pos.up()) >= 9) {
                for (int i = 0; i < 4; ++i) {
                    BlockPos blockpos = pos.add(random.nextInt(3) - 1, random.nextInt(5) - 3, random.nextInt(3) - 1);
                    if (worldIn.isBlockPresent(blockpos)) {
                        if (worldIn.getBlockState(blockpos).getBlock() == Blocks.GRASS_BLOCK && canGrassGrow(worldIn, blockpos)) {
                            worldIn.setBlockState(pos, Registrars.Blocks.GRASS_SLAB.getDefaultState().with(SNOWY, state.get(SNOWY)));
                            return;
                        }
                    }
                }
            }
        }
    }

    private static boolean canGrassGrow(IWorldReaderBase world, BlockPos pos) {
        BlockPos blockpos = pos.up();
        return world.getLight(blockpos) >= 4 && world.getBlockState(blockpos).getOpacity(world, blockpos) < world.getMaxLightLevel() && !world.getFluidState(blockpos).isTagged(FluidTags.WATER);
    }
}
