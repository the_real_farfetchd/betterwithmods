/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.ConstantProperties;
import com.betterwithmods.core.base.game.item.ItemArmor;
import com.betterwithmods.core.base.game.item.ItemAxe;
import com.betterwithmods.core.base.game.item.ItemPickaxe;
import com.betterwithmods.core.base.game.item.*;
import com.betterwithmods.core.base.registration.*;
import com.betterwithmods.mechanist.content.blocks.BlockMiningCharge;
import com.betterwithmods.mechanist.content.blocks.BlockVineTrap;
import com.betterwithmods.mechanist.content.blocks.bark.BlockBark;
import com.betterwithmods.mechanist.content.blocks.cooking.BlockCauldron;
import com.betterwithmods.mechanist.content.blocks.cooking.BlockCrucible;
import com.betterwithmods.mechanist.content.blocks.dynamic.furniture.BlockChair;
import com.betterwithmods.mechanist.content.blocks.dynamic.furniture.BlockTable;
import com.betterwithmods.mechanist.content.blocks.dynamic.mini.BlockCorner;
import com.betterwithmods.mechanist.content.blocks.dynamic.mini.BlockMoulding;
import com.betterwithmods.mechanist.content.blocks.dynamic.mini.BlockSiding;
import com.betterwithmods.mechanist.content.blocks.hemp.BlockHempBottom;
import com.betterwithmods.mechanist.content.blocks.hemp.BlockHempTop;
import com.betterwithmods.mechanist.content.blocks.mechanical.BlockAxle;
import com.betterwithmods.mechanist.content.blocks.mechanical.BlockHandcrank;
import com.betterwithmods.mechanist.content.blocks.mechanical.BlockMillstone;
import com.betterwithmods.mechanist.content.blocks.mechanical.BlockPulley;
import com.betterwithmods.mechanist.content.blocks.mechanical.generator.BlockCreativeGenerator;
import com.betterwithmods.mechanist.content.blocks.mechanical.generator.BlockHorizontalWindmill;
import com.betterwithmods.mechanist.content.blocks.mechanical.joint.BlockClutchbox;
import com.betterwithmods.mechanist.content.blocks.mechanical.joint.BlockClutchboxBroken;
import com.betterwithmods.mechanist.content.blocks.mechanical.joint.BlockGearbox;
import com.betterwithmods.mechanist.content.blocks.mechanical.joint.BlockGearboxBroken;
import com.betterwithmods.mechanist.content.blocks.pottery.BlockPlanter;
import com.betterwithmods.mechanist.content.blocks.pottery.PlanterType;
import com.betterwithmods.mechanist.content.blocks.redstone.BlockAdvancedObserver;
import com.betterwithmods.mechanist.content.blocks.redstone.BlockHibachi;
import com.betterwithmods.mechanist.content.blocks.rope.BlockAnchor;
import com.betterwithmods.mechanist.content.blocks.rope.BlockRope;
import com.betterwithmods.mechanist.content.blocks.slabs.BlockDirtSlab;
import com.betterwithmods.mechanist.content.blocks.slabs.BlockGrassPathSlab;
import com.betterwithmods.mechanist.content.blocks.slabs.BlockGrassSlab;
import com.betterwithmods.mechanist.content.blocks.storage.BlockPadding;
import com.betterwithmods.mechanist.content.entities.EntityDynamite;
import com.betterwithmods.mechanist.content.entities.EntityMiningCharge;
import com.betterwithmods.mechanist.content.items.ItemBlockCamo;
import com.betterwithmods.mechanist.content.items.ItemDynamite;
import com.betterwithmods.mechanist.content.items.ItemMattock;
import com.betterwithmods.mechanist.content.items.ItemSeedsHemp;
import com.betterwithmods.mechanist.content.items.armor.materials.BWMArmorMaterial;
import com.betterwithmods.mechanist.content.tiles.TileCamoflage;
import com.betterwithmods.mechanist.content.tiles.cooking.TileCauldron;
import com.betterwithmods.mechanist.content.tiles.cooking.TileCrucible;
import com.betterwithmods.mechanist.content.tiles.mechanical.TileAxle;
import com.betterwithmods.mechanist.content.tiles.mechanical.TilePulley;
import com.betterwithmods.mechanist.content.tiles.mechanical.generator.TileCreativeGenerator;
import com.betterwithmods.mechanist.content.tiles.mechanical.generator.TileHandcrank;
import com.betterwithmods.mechanist.content.tiles.mechanical.generator.TileHorizontalWindmill;
import com.betterwithmods.mechanist.content.tiles.mechanical.joint.TileClutchbox;
import com.betterwithmods.mechanist.content.tiles.mechanical.joint.TileGearbox;
import com.betterwithmods.mechanist.modules.DynamicBlocks;
import com.google.common.collect.Lists;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRedstoneLamp;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.color.BlockColors;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.entity.EntityType;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.*;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.GrassColors;
import net.minecraft.world.biome.BiomeColors;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.Logger;

import java.util.List;


@SuppressWarnings("ALL")
@Mod.EventBusSubscriber
public class Registrars {

    public static Registrars INSTANCE;

    public static final CreativeTab ITEM_GROUP = new CreativeTab(References.MODID_MECHANIST, "items", () -> new ItemStack(Items.HEMP_LEAF));

    public static final CreativeTab MINI_BLOCKS = new CreativeTab(References.MODID_MECHANIST, "miniblocks",
            () -> TileCamoflage.fromParent(Blocks.SIDING_WOOD, net.minecraft.init.Blocks.OAK_PLANKS.getDefaultState()));


    private static String MODID;


    private static IItemTier SOULFORGED_STEEL = new ToolTier(4, 2250, 10f, 3, 22, () -> Ingredient.fromItems(Items.SOULFORGED_STEEL_INGOT));

    public Registrars(String modid, Logger logger) {
        MODID = modid;
        INSTANCE = this;
        new BlockRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<Block> registry) {
                register(registry, "padding_block", new BlockPadding(ConstantProperties.WOOL));

                register(registry, "hemp_plant_bottom", new BlockHempBottom(ConstantProperties.PLANT_PROPERTIES));
                register(registry, "hemp_plant_top", new BlockHempTop(ConstantProperties.PLANT_PROPERTIES));
                register(registry, "cauldron", new BlockCauldron(ConstantProperties.CAULDRON));
                register(registry, "crucible", new BlockCrucible(ConstantProperties.CRUCIBLE));

                register(registry, "hibachi", new BlockHibachi(ConstantProperties.REDSTONE_DEVICE));
                register(registry, "advanced_observer", new BlockAdvancedObserver(Block.Properties.create(Material.ROCK).hardnessAndResistance(3.0F)));

                register(registry, "dirt_slab", new BlockDirtSlab(ConstantProperties.DIRT.needsRandomTick()));
                register(registry, "grass_slab", new BlockGrassSlab(ConstantProperties.GRASS.needsRandomTick()));
                register(registry, "grass_path_slab", new BlockGrassPathSlab(ConstantProperties.GRASS));

                register(registry, "mining_charge", new BlockMiningCharge(ConstantProperties.MINING_CHARGE));

                register(registry, "rope", new BlockRope(ConstantProperties.ROPE));
                register(registry, "anchor", new BlockAnchor(ConstantProperties.ANCHOR));

                register(registry, "vine_trap", new BlockVineTrap(ConstantProperties.PLANT_PROPERTIES));

                register(registry, "axle", new BlockAxle(ConstantProperties.WOOD));
                register(registry, "clutchbox", new BlockClutchbox(ConstantProperties.WOOD));
                register(registry, "gearbox", new BlockGearbox(ConstantProperties.WOOD));
                register(registry, "handcrank", new BlockHandcrank(ConstantProperties.ROCK));

                register(registry, "clutchbox_broken", new BlockClutchboxBroken(ConstantProperties.WOOD));
                register(registry, "gearbox_broken", new BlockGearboxBroken(ConstantProperties.WOOD));

                register(registry, "millstone", new BlockMillstone(ConstantProperties.ROCK));

                register(registry, "creative_generator", new BlockCreativeGenerator(ConstantProperties.WOOD));
                register(registry, "horizontal_windmill", new BlockHorizontalWindmill(ConstantProperties.WOOD));

                register(registry, "pulley", new BlockPulley(ConstantProperties.WOOD));

                //TODO flour, nether_sludge

                for (EnumDyeColor color : EnumDyeColor.values()) {
                    Blocks.REDSTONE_LAMPS.add(
                            register(registry, String.format("%s_redstone_lamp", color.getTranslationKey()), new BlockRedstoneLamp(ConstantProperties.LAMP))
                    );
                }

                for (PlanterType type : PlanterType.VALUES) {
                    Blocks.PLANTERS.add(
                            register(registry, String.format("%s_planter", type.getName()), new BlockPlanter(type, ConstantProperties.PLANTER))
                    );
                }

                ///Dynamic Blocks
                DynamicBlocks.addAll(
                        register(registry, "bark", new BlockBark(ConstantProperties.WOOD)),

                        register(registry, "table_wood", new BlockTable(ConstantProperties.WOOD)),
                        register(registry, "table_rock", new BlockTable(ConstantProperties.ROCK)),

                        register(registry, "siding_wood", new BlockSiding(ConstantProperties.WOOD)),
                        register(registry, "siding_rock", new BlockSiding(ConstantProperties.ROCK)),

                        register(registry, "moulding_wood", new BlockMoulding(ConstantProperties.WOOD)),
                        register(registry, "moulding_rock", new BlockMoulding(ConstantProperties.ROCK)),

                        register(registry, "corner_wood", new BlockCorner(ConstantProperties.WOOD)),
                        register(registry, "corner_rock", new BlockCorner(ConstantProperties.ROCK)),

                        register(registry, "chair_wood", new BlockChair(ConstantProperties.WOOD)),
                        register(registry, "chair_rock", new BlockChair(ConstantProperties.ROCK))
                );
            }


        };
        new ItemRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<Item> registry) {
                ItemProperties prop = ConstantProperties.MATERIALS.group(ITEM_GROUP);

                register(registry, "wooden_gear", new ItemBase(prop));
                register(registry, "hemp_seed", new ItemSeedsHemp(prop));
                register(registry, "hemp_leaf", new ItemBase(prop));
                register(registry, "hemp_fiber", new ItemBase(prop));
                register(registry, "filament", new ItemBase(prop));
                register(registry, "element", new ItemBase(prop));
                register(registry, "concentrated_hellfire", new ItemBase(prop));
                register(registry, "tallow", new ItemBase(prop));
                register(registry, "leather_slice", new ItemBase(prop));
                register(registry, "tanned_leather", new ItemBase(prop));
                register(registry, "tanned_leather_belt", new ItemBase(prop));
                register(registry, "tanned_leather_strap", new ItemBase(prop));
                register(registry, "tanned_leather_slice", new ItemBase(prop));
                register(registry, "scoured_leather", new ItemBase(prop));
                register(registry, "scoured_leather_slice", new ItemBase(prop));
                register(registry, "soulforged_steel_ingot", new ItemBase(prop));
                register(registry, "soulforged_steel_nugget", new ItemBase(prop));
                register(registry, "soulforged_armor_plate", new ItemBase(prop));
                register(registry, "diamond_ingot", new ItemBase(prop));
                register(registry, "blasting_oil", new ItemBase(prop));
                register(registry, "saw_dust", new ItemBase(prop));
                register(registry, "fuse", new ItemBase(prop));
                register(registry, "glue", new ItemBase(prop));
                register(registry, "soap", new ItemBase(prop));
                register(registry, "brimstone", new ItemBase(prop));
                register(registry, "coal_dust", new ItemBase(prop));
                register(registry, "ender_slag", new ItemBase(prop));
                register(registry, "hellfire_dust", new ItemBase(prop));
                register(registry, "haft", new ItemBase(prop));
                register(registry, "padding", new ItemBase(prop));
                register(registry, "chain_mail", new ItemBase(prop));
                register(registry, "broadhead", new ItemBase(prop));
                register(registry, "redstone_latch", new ItemBase(prop));
                register(registry, "soul_flux", new ItemBase(prop));
                register(registry, "niter", new ItemBase(prop));
                register(registry, "polished_lapis", new ItemBase(prop));
                register(registry, "potash", new ItemBase(prop));
                register(registry, "soul_dust", new ItemBase(prop));
                register(registry, "ender_ocular", new ItemBase(prop));
                register(registry, "iron_screw", new ItemBase(prop));
                register(registry, "waterwheel_paddle", new ItemBase(prop));
                register(registry, "wind_sail", new ItemBase(prop));
                register(registry, "nethercoal", new ItemBase(ConstantProperties.NETHERCOAL));


                register(registry, "soulforged_steel_armor_helm", new ItemArmor(BWMArmorMaterial.SOULFORGED_STEEL, EntityEquipmentSlot.HEAD, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_armor_chest", new ItemArmor(BWMArmorMaterial.SOULFORGED_STEEL, EntityEquipmentSlot.CHEST, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_armor_legs", new ItemArmor(BWMArmorMaterial.SOULFORGED_STEEL, EntityEquipmentSlot.LEGS, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_armor_feet", new ItemArmor(BWMArmorMaterial.SOULFORGED_STEEL, EntityEquipmentSlot.FEET, new ItemProperties().group(ITEM_GROUP)));

                register(registry, "tanned_leather_armor_helm", new ItemArmor(BWMArmorMaterial.TANNED_LEATHER, EntityEquipmentSlot.HEAD, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "tanned_leather_armor_chest", new ItemArmor(BWMArmorMaterial.TANNED_LEATHER, EntityEquipmentSlot.CHEST, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "tanned_leather_armor_legs", new ItemArmor(BWMArmorMaterial.TANNED_LEATHER, EntityEquipmentSlot.LEGS, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "tanned_leather_armor_feet", new ItemArmor(BWMArmorMaterial.TANNED_LEATHER, EntityEquipmentSlot.FEET, new ItemProperties().group(ITEM_GROUP)));

                register(registry, "wool_armor_helm", new ItemArmor(BWMArmorMaterial.WOOL, EntityEquipmentSlot.HEAD, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "wool_armor_chest", new ItemArmor(BWMArmorMaterial.WOOL, EntityEquipmentSlot.CHEST, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "wool_armor_legs", new ItemArmor(BWMArmorMaterial.WOOL, EntityEquipmentSlot.LEGS, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "wool_armor_feet", new ItemArmor(BWMArmorMaterial.WOOL, EntityEquipmentSlot.FEET, new ItemProperties().group(ITEM_GROUP)));

                register(registry, "dynamite", new ItemDynamite(prop));


                ItemTiered pickaxe = (ItemTiered) register(registry, "soulforged_steel_pickaxe", new ItemPickaxe(SOULFORGED_STEEL, 1, -2.8F, new ItemProperties().group(ITEM_GROUP)));
                ItemTiered shovel = (ItemTiered) register(registry, "soulforged_steel_shovel", new ItemSpade(SOULFORGED_STEEL, 1.5F, -3.0F, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_sword", new ItemSword(SOULFORGED_STEEL, 4, -2.4F, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_axe", new ItemAxe(SOULFORGED_STEEL, 5.0F, -3.0F, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_hoe", new ItemHoe(SOULFORGED_STEEL, 0, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_mattock", new ItemMattock(SOULFORGED_STEEL, 1, -2.8F, new ItemProperties().group(ITEM_GROUP)));
                register(registry, "soulforged_steel_battleaxe", new ItemAxe(SOULFORGED_STEEL, 5, -2.4F, new ItemProperties().group(ITEM_GROUP)));


                for (Block block : Blocks.REDSTONE_LAMPS) {
                    register(registry, block, ITEM_GROUP);
                }
                for (Block block : Blocks.PLANTERS) {
                    register(registry, block, ITEM_GROUP);
                }

                register(registry, Blocks.VINE_TRAP, ITEM_GROUP);
                register(registry, Blocks.HIBACHI, ITEM_GROUP);
                register(registry, Blocks.ADVANCED_OBSERVER, ITEM_GROUP);
                register(registry, Blocks.CAULDRON, ITEM_GROUP);
                register(registry, Blocks.CRUCIBLE, ITEM_GROUP);
                register(registry, Blocks.DIRT_SLAB, ITEM_GROUP);
                register(registry, Blocks.GRASS_SLAB, ITEM_GROUP);
                register(registry, Blocks.MINING_CHARGE, ITEM_GROUP);
                register(registry, Blocks.ROPE, ITEM_GROUP);
                register(registry, Blocks.ANCHOR, ITEM_GROUP);
                register(registry, Blocks.AXLE, ITEM_GROUP);
                register(registry, Blocks.CLUTCHBOX, ITEM_GROUP);
                register(registry, Blocks.GEARBOX, ITEM_GROUP);
                register(registry, Blocks.CLUTCHBOX_BROKEN, ITEM_GROUP);
                register(registry, Blocks.GEARBOX_BROKEN, ITEM_GROUP);
                register(registry, Blocks.MILLSTONE, ITEM_GROUP);
                register(registry, Blocks.CREATIVE_GENERATOR, ITEM_GROUP);
                register(registry, Blocks.HORIZONTAL_WINDMILL, ITEM_GROUP);
                register(registry, Blocks.HANDCRANK, ITEM_GROUP);
                register(registry, Blocks.PADDING_BLOCK, ITEM_GROUP);
                register(registry, Blocks.PULLEY, ITEM_GROUP);


                register(registry, ItemBlockCamo::new, Blocks.BARK, MINI_BLOCKS);
                register(registry, ItemBlockCamo::new, Blocks.TABLE_WOOD, MINI_BLOCKS);
                register(registry, ItemBlockCamo::new, Blocks.TABLE_ROCK, MINI_BLOCKS);
                register(registry, ItemBlockCamo::new, Blocks.SIDING_WOOD, MINI_BLOCKS);
                register(registry, ItemBlockCamo::new, Blocks.SIDING_ROCK, MINI_BLOCKS);
                register(registry, ItemBlockCamo::new, Blocks.MOULDING_WOOD, MINI_BLOCKS);
                register(registry, ItemBlockCamo::new, Blocks.MOULDING_ROCK, MINI_BLOCKS);
                register(registry, ItemBlockCamo::new, Blocks.CORNER_WOOD, MINI_BLOCKS);
                register(registry, ItemBlockCamo::new, Blocks.CORNER_ROCK, MINI_BLOCKS);
                register(registry, ItemBlockCamo::new, Blocks.CHAIR_WOOD, MINI_BLOCKS);
                register(registry, ItemBlockCamo::new, Blocks.CHAIR_ROCK, MINI_BLOCKS);
            }
        };

        new EntityRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<EntityType<?>> registry) {
                register(registry, "betterwithmods_mechanist:mining_charge", EntityType.Builder
                        .create(EntityMiningCharge.class, EntityMiningCharge::new)
                        .tracker(64, 1, true)
                );

                register(registry, "betterwithmods_mechanist:dynamite", EntityType.Builder
                        .create(EntityDynamite.class, EntityDynamite::new)
                        .tracker(64, 1, true)
                );
            }
        };

        new TileEntityRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<TileEntityType<?>> registry) {
                register(registry, "camoflage", Tiles.TILE_CAMOFLAGE);

                register(registry, "axle", Tiles.TILE_AXLE);
                register(registry, "creative_generator", Tiles.TILE_CREATIVE_GENERATOR);
                register(registry, "horizontal_windmill", Tiles.TILE_HORIZONTAL_WINDMILL);

                register(registry, "gearbox", Tiles.TILE_GEARBOX);
                register(registry, "clutchbox", Tiles.TILE_CLUTCHBOX);
                register(registry, "handcrank", Tiles.TILE_HANDCRANK);

                register(registry, "cauldron", Tiles.TILE_CAULDRON);
                register(registry, "crucible", Tiles.TILE_CRUCIBLE);
                register(registry, "pulley", Tiles.TILE_PULLEY);
            }
        };

        new SoundRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<SoundEvent> registry) {
                register(registry, "block.mechanical.creak");
            }
        };


    }

    @SubscribeEvent
    public static void onBlockColorsInit(ColorHandlerEvent.Block event) {
        BlockColors colors = event.getBlockColors();
        colors.register((state, world, pos, tintIndex) -> world != null && pos != null ? BiomeColors.getGrassColor(world, pos) : GrassColors.get(0.5D, 1.0D),
                Blocks.GRASS_SLAB,
                Blocks.GRASS_PLANTER,
                Blocks.VINE_TRAP
        );
        colors.register((state, world, pos, tintIndex) -> world != null && pos != null ? BiomeColors.getWaterColor(world, pos) : -1,
                Registrars.Blocks.WATER_PLANTER);
    }


    @SubscribeEvent
    public static void onItemColorsInit(ColorHandlerEvent.Item event) {
        ItemColors itemColors = event.getItemColors();
        BlockColors blockColors = event.getBlockColors();

        itemColors.register((stack, tintIndex) -> {
                    IBlockState iblockstate = ((ItemBlock) stack.getItem()).getBlock().getDefaultState();
                    return blockColors.getColor(iblockstate, null, null, tintIndex);
                },
                Blocks.GRASS_PLANTER,
                Blocks.GRASS_SLAB,
                Blocks.VINE_TRAP
        );

        itemColors.register((stack, tintIndex) -> 4159204, Blocks.WATER_PLANTER);
    }


    @ObjectHolder(References.MODID_MECHANIST)
    public static class Blocks {
        public static final Block HEMP_PLANT_BOTTOM = null;
        public static final Block HEMP_PLANT_TOP = null;
        public static final Block HIBACHI = null;

        public static final Block CAULDRON = null;
        public static final Block CRUCIBLE = null;

        public static final Block ADVANCED_OBSERVER = null;

        public static List<Block> REDSTONE_LAMPS = Lists.newArrayList();
        public static List<Block> PLANTERS = Lists.newArrayList();

        public static final Block ANCHOR = null;
        public static final Block ROPE = null;
        public static final Block MINING_CHARGE = null;

        public static final Block EMPTY_PLANTER = null;
        public static final Block GRASS_PLANTER = null;
        public static final Block WATER_PLANTER = null;

        public static final Block DIRT_SLAB = null;
        public static final Block GRASS_SLAB = null;
        public static final Block GRASS_PATH_SLAB = null;
        public static final Block MYCELIUM_SLAB = null;

        public static final Block VINE_TRAP = null;

        public static final Block BARK = null;

        public static final Block TABLE_WOOD = null;
        public static final Block TABLE_ROCK = null;

        public static final Block SIDING_WOOD = null;
        public static final Block SIDING_ROCK = null;

        public static final Block MOULDING_WOOD = null;
        public static final Block MOULDING_ROCK = null;

        public static final Block CORNER_WOOD = null;
        public static final Block CORNER_ROCK = null;

        public static final Block CHAIR_WOOD = null;
        public static final Block CHAIR_ROCK = null;

        public static final Block AXLE = null;
        public static final Block CLUTCHBOX = null;
        public static final Block GEARBOX = null;
        public static final Block CLUTCHBOX_BROKEN = null;
        public static final Block GEARBOX_BROKEN = null;
        public static final Block MILLSTONE = null;

        public static final Block CREATIVE_GENERATOR = null;
        public static final Block HORIZONTAL_WINDMILL = null;
        public static final Block HANDCRANK = null;

        public static final Block PADDING_BLOCK = null;

        public static final Block PULLEY = null;
    }


    @ObjectHolder(References.MODID_MECHANIST)
    public static class Items {
        public static final Item HEMP_SEED = null;
        public static final Item HEMP_LEAF = null;
        public static final Item HEMP_FIBER = null;
        public static final Item HEMP_CLOTH = null;

        public static final Item WOODEN_GEAR = null;
        public static final Item FILAMENT = null;
        public static final Item ELEMENT = null;
        public static final Item CONCENTRATED_HELLFIRE = null;
        public static final Item SOULFORGED_STEEL_INGOT = null;
        public static final Item SOULFORGED_STEEL_NUGGET = null;
        public static final Item SOAP = null;
        public static final Item BRIMSTONE = null;
        public static final Item COAL_DUST = null;
        public static final Item ENDER_SLAG = null;
        public static final Item HELLFIRE_DUST = null;
        public static final Item HAFT = null;
        public static final Item PADDING = null;
        public static final Item TANNED_LEATHER = null;
        public static final Item SAW_DUST = null;

        public static final Item DYNAMITE = null;

        public static final Item SOULFORGED_STEEL_ARMOR_HELM = null;
        public static final Item SOULFORGED_STEEL_ARMOR_CHEST = null;
        public static final Item SOULFORGED_STEEL_ARMOR_LEGS = null;
        public static final Item SOULFORGED_STEEL_ARMOR_FEET = null;

        public static final Item WOOL_ARMOR_HELM = null;
        public static final Item WOOL_ARMOR_CHEST = null;
        public static final Item WOOL_ARMOR_LEGS = null;
        public static final Item WOOL_ARMOR_FEET = null;

        public static final Item SOULFORGED_STEEL_PICKAXE = null;
        public static final Item SOULFORGED_STEEL_SHOVEL = null;
    }

    @ObjectHolder(References.MODID_MECHANIST)
    public static class Entities {
        public static final EntityType<EntityMiningCharge> MINING_CHARGE = null;
        public static final EntityType<EntityDynamite> DYNAMITE = null;
    }

    public static class Tiles {
        public static TileEntityType<TileCamoflage> TILE_CAMOFLAGE = TileEntityType.Builder.create(TileCamoflage::new).build(null);
        public static TileEntityType<TileAxle> TILE_AXLE = TileEntityType.Builder.create(TileAxle::new).build(null);

        public static TileEntityType<TileCreativeGenerator> TILE_CREATIVE_GENERATOR = TileEntityType.Builder.create(TileCreativeGenerator::new).build(null);
        public static TileEntityType<TileHorizontalWindmill> TILE_HORIZONTAL_WINDMILL = TileEntityType.Builder.create(TileHorizontalWindmill::new).build(null);

        public static TileEntityType<TileGearbox> TILE_GEARBOX = TileEntityType.Builder.create(TileGearbox::new).build(null);
        public static TileEntityType<TileClutchbox> TILE_CLUTCHBOX = TileEntityType.Builder.create(TileClutchbox::new).build(null);
        public static TileEntityType<TileHandcrank> TILE_HANDCRANK = TileEntityType.Builder.create(TileHandcrank::new).build(null);

        public static TileEntityType<TileCrucible> TILE_CRUCIBLE = TileEntityType.Builder.create(TileCrucible::new).build(null);

        public static TileEntityType<TileCauldron> TILE_CAULDRON = TileEntityType.Builder.create(TileCauldron::new).build(null);

        public static TileEntityType<TilePulley> TILE_PULLEY = TileEntityType.Builder.create(TilePulley::new).build(null);
    }


    public static class Sounds {
        @ObjectHolder(References.MODID_MECHANIST + ":block.mechanical.creak")
        public static final SoundEvent MECHANICAL_CREAK = null;
    }
}
