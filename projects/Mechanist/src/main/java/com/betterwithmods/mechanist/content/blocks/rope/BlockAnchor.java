/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.rope;

import com.betterwithmods.core.api.blocks.IRopeConnector;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.PlayerUtils;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.handlers.RopeHandler;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.*;

import javax.annotation.Nullable;

public class BlockAnchor extends BlockBase implements IRopeConnector {

    private static final DirectionProperty FACING = BlockStateProperties.FACING;
    private static final BooleanProperty CONNECTED = BooleanProperty.create("connected");

    private static final VoxelShape ROPE_BOTTOM = Block.makeCuboidShape(7, 0, 7, 9, 6, 9);
    private static final VoxelShape ROPE_TOP = Block.makeCuboidShape(7, 10, 7, 9, 16, 9);


    public BlockAnchor(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(FACING, EnumFacing.UP).with(CONNECTED, false));
        setIsFullCube(state -> false);
        setRenderLayer(BlockRenderLayer.CUTOUT);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        builder.add(FACING);
        builder.add(CONNECTED);
    }

    @Nullable
    @Override
    public IBlockState getStateForPlacement(BlockItemUseContext context) {
        return getDefaultState().with(FACING, context.getFace());
    }

    private boolean isConnector(IWorld world, BlockPos pos, EnumFacing facing) {
        return world.getBlockState(pos.offset(facing)).getBlock() instanceof IRopeConnector;
    }

    @Override
    public IBlockState updatePostPlacement(IBlockState state, EnumFacing facing, IBlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        boolean connected;

        if (state.get(FACING) == EnumFacing.UP) {
            connected = isConnector(worldIn, currentPos, EnumFacing.UP);
        } else {
            connected = isConnector(worldIn, currentPos, EnumFacing.DOWN);
        }

        if (connected) {
            return state.with(CONNECTED, true);
        }
        return state.with(CONNECTED, false);
    }

    @Override
    public boolean canConnect(IWorldReaderBase worldReader, BlockPos pos) {
        IBlockState state = worldReader.getBlockState(pos);
        if (state.getBlock() == this)
            return state.get(FACING) != EnumFacing.UP;
        return false;
    }

    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader world, BlockPos pos) {
        EnumFacing facing = state.get(FACING);
        boolean connected = state.get(CONNECTED);
        int i = facing.getIndex();
        return connected ? SHAPES_ROPE[i] : SHAPES[i];
    }

    @Override
    public VoxelShape getCollisionShape(IBlockState state, IBlockReader world, BlockPos pos) {
        EnumFacing facing = state.get(FACING);
        return SLAB_SHAPES[facing.getIndex()];
    }

    @Override
    public boolean onBlockActivated(IBlockState state, World worldIn, BlockPos pos, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (hand != EnumHand.MAIN_HAND)
            return false;

        ItemStack held = player.getHeldItem(hand);

        if (held.isEmpty()) {
            if (player.isSneaking()) {
                BlockPos lowest = RopeHandler.findLowestRope(worldIn, pos);
                IBlockState lowestState = worldIn.getBlockState(lowest);
                if (lowestState.getBlock() == Registrars.Blocks.ROPE) {
                    worldIn.playEvent(2001, lowest, Block.getStateId(lowestState));
                    worldIn.removeBlock(lowest);
                    PlayerUtils.givePlayer(player, new ItemStack(Registrars.Blocks.ROPE));
                    return true;
                }
            }
        } else if (held.getItem() == Registrars.Blocks.ROPE.asItem()) {
            return RopeHandler.placeLowestRope(worldIn, pos, player, held);
        }
        return false;
    }

    @Override
    public boolean isLadder(IBlockState state, IWorldReader world, BlockPos pos, EntityLivingBase entity) {
        return state.get(FACING) != EnumFacing.UP;
    }


    private static VoxelShape[] SHAPES_ROPE = new VoxelShape[6], SHAPES = new VoxelShape[6];

    private static VoxelShape[] ANCHOR_SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
            Block.makeCuboidShape(6, 6, 6, 10, 10, 10),
    };

    private static VoxelShape[] SLAB_SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(0, 10, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 6, 16),
            Block.makeCuboidShape(0, 0, 10, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 6),
            Block.makeCuboidShape(10, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 6, 16, 16),
    };

    static {
        for (int i = 0; i < SHAPES_ROPE.length; i++) {
            VoxelShape main = VoxelShapes.combineAndSimplify(SLAB_SHAPES[i], ANCHOR_SHAPES[i], IBooleanFunction.OR);
            SHAPES[i] = main;
            if(i == 1) {
                SHAPES_ROPE[i] = VoxelShapes.combineAndSimplify(main, ROPE_TOP, IBooleanFunction.OR);
            } else {
                SHAPES_ROPE[i] = VoxelShapes.combineAndSimplify(main, ROPE_BOTTOM, IBooleanFunction.OR);
            }
        }
    }
}
