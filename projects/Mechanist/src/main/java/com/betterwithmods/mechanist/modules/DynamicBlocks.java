/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.modules;

import com.betterwithmods.core.base.game.block.MaterialKey;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.mechanist.client.baking.CamoFactory;
import com.betterwithmods.mechanist.content.blocks.dynamic.BlockVariant;
import com.betterwithmods.mechanist.content.blocks.dynamic.TagVariant;
import com.betterwithmods.mechanist.content.blocks.dynamic.VariantProvider;
import com.betterwithmods.mechanist.content.tiles.TileCamoflage;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;

import java.util.*;
import java.util.stream.Collectors;

public class DynamicBlocks extends ModuleBase {

    private final static Collection<VariantProvider> PROVIDERS = Lists.newArrayList();

    private final static Set<ResourceLocation> DYNAMICBLOCKS = Sets.newHashSet();
    public static void addAll(Block... blocks) {
        DYNAMICBLOCKS.addAll(Arrays.stream(blocks).map(Block::getRegistryName).collect(Collectors.toSet()));
    }
    @SuppressWarnings("UnstableApiUsage")
    private final static ListMultimap<MaterialKey, IBlockState> MATERIAL_VARIANTS = MultimapBuilder.hashKeys().arrayListValues().build();


    private ForgeConfigSpec.ConfigValue<List<? extends String>> variants;

    public DynamicBlocks() {
        super("dynamic_blocks");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        this.variants = builder.comment("The list of sources from which dynamic block will get variants. Ex. tag:minecraft:planks or block:minecraft:cobblestone")
                .translation("config.betterwithmods_mechanist.variants")
                .defineList("variants",
                        Lists.newArrayList("tag:minecraft:planks", "tag:forge:stone"),
                        o -> true
                );
    }

    @Override
    public void loadComplete(FMLLoadCompleteEvent event) {
        for(String variant: variants.get()) {
            PROVIDERS.add(deserialize(variant));
        }
    }

    public static Collection<IBlockState> filter(MaterialKey material) {
        return PROVIDERS.stream()
                .map(VariantProvider::variants)
                .flatMap(Collection::stream)
                .filter(state -> material.matches(state.getMaterial()))
                .collect(Collectors.toList());
    }

    public static Collection<IBlockState> getVariants(IBlockState state) {
        MaterialKey material = MaterialKey.get(state.getMaterial());
        if (!MATERIAL_VARIANTS.containsKey(material)) {
            MATERIAL_VARIANTS.putAll(material, filter(material));
        }
        return MATERIAL_VARIANTS.get(material);
    }


    public static void fillVariants(Block block, NonNullList<ItemStack> items) {
        Collection<IBlockState> variants = getVariants(block.getDefaultState());
        for (IBlockState state : variants) {
            items.add(TileCamoflage.fromParent(block, state));
        }
    }

    @Override
    public String getDescription() {
        return "Dynamically add various building block to the game";
    }


    public static VariantProvider deserialize(String variant) {
        String[] parts = variant.split(":");

        String type = "block";
        ResourceLocation location = null;
        if (parts.length == 3) {
            type = parts[0];
            location = new ResourceLocation(parts[1], parts[2]);
        } else if (parts.length == 2) {
            location = new ResourceLocation(parts[0], parts[1]);
        }
        if (location != null) {
            switch (type) {
                case "block":
                    return new BlockVariant(location);
                case "tag":
                    return new TagVariant(location);
            }
        }
        return null;
    }

    @SubscribeEvent
    public void modelBake(ModelBakeEvent event) {
        Map<ModelResourceLocation, IBakedModel> registry = event.getModelRegistry();
        for(ModelResourceLocation location: registry.keySet()) {
            ResourceLocation block = new ResourceLocation(location.getNamespace(), location.getPath());
            if(DYNAMICBLOCKS.contains(block)) {
                IBakedModel originalModel = registry.get(location);
                registry.put(location, new CamoFactory(originalModel));
            }
        }
    }

}
