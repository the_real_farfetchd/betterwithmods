/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.entities;

import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.handlers.MiningChargeHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.MoverType;
import net.minecraft.init.Particles;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class EntityMiningCharge extends Entity {

    private static final DataParameter<Integer> FUSE;
    private static final DataParameter<EnumFacing> FACING;

    @Nullable
    private EntityLivingBase tntPlacedBy;

    private int fuse;
    private EnumFacing facing;

    public EntityMiningCharge(World world) {
        super(Registrars.Entities.MINING_CHARGE, world);
        this.fuse = 80;
        this.preventEntitySpawning = true;
        this.isImmuneToFire = true;
        this.setSize(0.98F, 0.98F);
        setNoGravity(true);
    }

    public EntityMiningCharge(World world, EnumFacing facing, Vec3d pos, @Nullable EntityLivingBase player) {
        this(world);
        this.setFacing(facing);
        this.setPosition(pos.x,pos.y,pos.z);
        this.setFuse(80);
        this.motionX = 0;
        this.motionY = 0;
        this.motionZ = 0;
        this.prevPosX = pos.x;
        this.prevPosY = pos.y;
        this.prevPosZ = pos.z;
        this.tntPlacedBy = player;
    }

    protected void registerData() {
        this.dataManager.register(FUSE, 80);
        this.dataManager.register(FACING, EnumFacing.UP);
    }

    protected boolean canTriggerWalking() {
        return false;
    }

    public boolean canBeCollidedWith() {
        return !this.isAlive();
    }

    public void tick() {
        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;
        if (!this.hasNoGravity()) {
            this.motionY -= 0.03999999910593033D;
        }

        this.move(MoverType.SELF, this.motionX, this.motionY, this.motionZ);

        --this.fuse;
        if (this.fuse <= 0) {
            this.remove();
            if (!this.world.isRemote) {
                this.explode();
            }
        } else {
            this.handleWaterMovement();
            this.world.spawnParticle(Particles.SMOKE, this.posX, this.posY + 0.5D, this.posZ, 0.0D, 0.0D, 0.0D);
        }

    }

    private void explode() {
        MiningChargeHandler handler = new MiningChargeHandler();
        handler.onExplode(world, getPosition(), facing);
    }

    protected void writeAdditional(NBTTagCompound tag) {
        tag.setShort("Fuse", (short) this.getFuse());
        tag.setShort("Facing", (short) this.getFacing().getIndex());
    }

    protected void readAdditional(NBTTagCompound tag) {
        this.setFuse(tag.getShort("Fuse"));
        this.setFacing(EnumFacing.byIndex(tag.getShort("Facing")));
    }

    @Nullable
    public EntityLivingBase getTntPlacedBy() {
        return this.tntPlacedBy;
    }

    public float getEyeHeight() {
        return 0.0F;
    }

    public void setFacing(EnumFacing facing) {
        this.dataManager.set(FACING, facing);
        this.facing = facing;
    }

    public void setFuse(int fuse) {
        this.dataManager.set(FUSE, fuse);
        this.fuse = fuse;
    }

    public EnumFacing getFacing() {
        return this.facing;
    }

    public void notifyDataManagerChange(DataParameter<?> parameter) {
        if (FUSE.equals(parameter)) {
            this.fuse = this.dataManager.get(FUSE);
        }
        if (FACING.equals(parameter)) {
            this.facing = this.dataManager.get(FACING);
        }
    }

    public int getFuse() {
        return this.fuse;
    }

    static {
        FUSE = EntityDataManager.createKey(EntityMiningCharge.class, DataSerializers.VARINT);
        FACING = EntityDataManager.createKey(EntityMiningCharge.class, DataSerializers.FACING);
    }
}

