/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.creature.squid;

import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.utilties.ConfigUtils;
import com.betterwithmods.core.utilties.ReflectionHelper;
import com.betterwithmods.phenomenons.modules.creature.squid.ai.*;
import com.google.common.collect.Lists;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.passive.EntitySquid;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.List;

import static com.betterwithmods.core.utilties.ConfigUtils.validateStringToEntityClass;

public class HostileSquids extends ModuleBase {

    private ForgeConfigSpec.ConfigValue<List<? extends String>> squidHostileList;
    private ForgeConfigSpec.ConfigValue<Boolean> squidTargetPlayers;

    public HostileSquids() {
        super("hostileSquids");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        this.squidHostileList = builder.comment("The list of entities that squids will attack.")
                .translation("config.betterwithmods_phenomenons.squidHostileList")
                .defineList("squidHostileTowards", Lists.newArrayList("minecraft:cow", "minecraft:pig", "minecraft:sheep", "minecraft:llama", "minecraft:chicken"), o -> validateStringToEntityClass(o, EntityCreature.class));
        this.squidTargetPlayers = builder.comment("Decide whether squids will target players")
                .translation("config.betterwithmods_phenomenons.squidHostileToPlayers")
                .define("squidHostileToPlayers", true);
    }

    @SubscribeEvent
    public void onConstruction(EntityEvent.EntityConstructing event) {
        if(event.getEntity() instanceof EntitySquid) {
            EntitySquid entity = (EntitySquid) event.getEntity();
            entity.getAttributeMap().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(2f);
        }
    }

    @SubscribeEvent
    public void addAI(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof EntitySquid) {
            EntitySquid entity = (EntitySquid) event.getEntity();
            //Give squid navigator
            ReflectionHelper.setValue(EntityLiving.class, entity, "navigator", new PathNavigateSquid(entity, entity.world));
            //Remove all AI
            entity.tasks.taskEntries.clear();

            ConfigUtils.getEntities(squidHostileList).forEach(clazz -> entity.targetTasks.addTask(0, new AISquidTarget(entity, clazz, true)));

            if(squidTargetPlayers.get()) {
                entity.targetTasks.addTask(0, new AISquidTarget(entity, EntityPlayer.class, true));
            }

            entity.tasks.addTask(1, new AIMountPrey(entity, 1, false));
            entity.tasks.addTask(2, new AISquidAttack(entity, 1, false));
            entity.tasks.addTask(3, new AIMoveTowards(entity, 1, 16));
            entity.tasks.addTask(5, new AIMoveRandom(entity));
        }
    }

    @Override
    public String getDescription() {
        return "Squids are now a hostile mobs with some interesting properties.";
    }
}
