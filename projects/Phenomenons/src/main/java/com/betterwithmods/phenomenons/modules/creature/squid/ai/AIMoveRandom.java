/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.creature.squid.ai;

import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.passive.EntitySquid;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.EnumLightType;
import net.minecraft.world.World;

public class AIMoveRandom extends EntityAIBase {
    private final EntitySquid squid;

    public AIMoveRandom(EntitySquid squid) {
        this.squid = squid;
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute() {
        return squid.getAttackTarget() == null;
    }

    /**
     * Keep ticking a continuous task that has already been started
     */
    public void tick() {
        int i = this.squid.getIdleTime();
        if (i > 100) {
            this.squid.setMovementVector(0.0F, 0.0F, 0.0F);
        } else if (this.squid.getRNG().nextInt(50) == 0 || !this.squid.isInWater() || !this.squid.hasMovementVector()) {
            World world = this.squid.world;


            //Force squids deep underwater during daytime
            float f2 = -0.1F + this.squid.getRNG().nextFloat() * 0.2F;
            int sunLight = world.getLightFor(EnumLightType.SKY, this.squid.getPosition());
            if (sunLight > 8) {
                f2 = -Math.abs(f2);
            }
            //

            float f = this.squid.getRNG().nextFloat() * ((float) Math.PI * 2F);
            float f1 = MathHelper.cos(f) * 0.2F;
            float f3 = MathHelper.sin(f) * 0.2F;
            this.squid.setMovementVector(f1, f2, f3);
        }

    }
}
