/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.creature;

import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.utilties.ConfigUtils;
import com.google.common.collect.Lists;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.List;

import static com.betterwithmods.core.utilties.ConfigUtils.validateStringToEntityClass;

public class HostilesHuntPassives extends ModuleBase {

    private ForgeConfigSpec.ConfigValue<List<? extends String>> zombieHostileList, spiderHostileList, skeletonHostileList;

    public HostilesHuntPassives() {
        super("hostilesHuntPassives");
    }


    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        getLoader().addCondition("hostilesHuntPassives", this::isEnabled);
        this.zombieHostileList = builder.comment("The list of entities that zombies will attack.")
                .translation("config.betterwithmods_phenomenons.zombieHostileList")
                .defineList("zombieHostileToward", Lists.newArrayList("minecraft:cow", "minecraft:pig", "minecraft:sheep", "minecraft:llama"), o -> validateStringToEntityClass(o, EntityCreature.class));
        this.spiderHostileList = builder.comment("The list of entities that spiders will attack.")
                .translation("config.betterwithmods_phenomenons.spiderHostileList")
                .defineList("spiderHostileToward", Lists.newArrayList("minecraft:chicken"), o -> validateStringToEntityClass(o, EntityCreature.class));
        this.skeletonHostileList = builder.comment("The list of entities that skeletons will attack.")
                .translation("config.betterwithmods_phenomenons.skeletonHostileList")
                .defineList("skeletonHostileList", Lists.newArrayList("minecraft:bat"), o -> validateStringToEntityClass(o, EntityCreature.class));
    }


    @Override
    public String getDescription() {
        return "Hostile mobs will actively seek out and kill passive mobs in the world.";
    }

    @SubscribeEvent
    public void addAI(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof EntityZombie) {
            EntityZombie entity = (EntityZombie) event.getEntity();
            ConfigUtils.getEntities(zombieHostileList).forEach(clazz -> entity.targetTasks.addTask(4, new EntityAINearestAttackableTarget<>(entity, clazz, false)));
        } else if (event.getEntity() instanceof EntitySpider) {
            EntitySpider entity = (EntitySpider) event.getEntity();
            ConfigUtils.getEntities(spiderHostileList).forEach(clazz -> entity.targetTasks.addTask(4, new EntityAINearestAttackableTarget<>(entity, clazz, false)));
        } else if (event.getEntity() instanceof EntitySkeleton) {
            EntitySkeleton entity = (EntitySkeleton) event.getEntity();
            ConfigUtils.getEntities(skeletonHostileList).forEach(clazz -> entity.targetTasks.addTask(4, new EntityAINearestAttackableTarget<>(entity, clazz, false)));
        }
    }

}
