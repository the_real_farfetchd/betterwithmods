/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.arcanium.content.items;

import com.betterwithmods.arcanium.Registrars;
import com.betterwithmods.arcanium.content.blocks.BlockVoidProxy;
import com.betterwithmods.core.base.game.item.ItemBase;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Collection;
import java.util.Set;
import java.util.function.BiFunction;

public class ItemVoidHole extends ItemBase {



    public ItemVoidHole(Properties properties) {
        super(properties);
        //TODO make distance and time attributes
    }

    @Override
    public EnumActionResult onItemUse(ItemUseContext context) {
        BlockPos pos = context.getPos();
        World world = context.getWorld();
        EnumFacing facing = context.getFace();
        IBlockState state = world.getBlockState(pos);
        EnvironmentUtils.playSound(world, pos, SoundEvents.ENTITY_ENDERMAN_TELEPORT, SoundCategory.PLAYERS, 1, 1);
        if (!world.isAirBlock(pos)) {
            int baseTime = 20 * 5;
            int distance = 10;
            Iterable<BlockPos.MutableBlockPos> positions = getOffsetsAround(pos, facing.getOpposite(), distance);
            positions.forEach(p -> BlockVoidProxy.setProxyState(Registrars.Blocks.VOID_PROXY, world, p, baseTime));
        }
        return super.onItemUse(context);
    }

    private static final BlockPos[][] OFFSETS_BY_AXIS = new BlockPos[][]{
            generateRing((i, j) -> new BlockPos(0, i, j)), //X
            generateRing((i, j) -> new BlockPos(i, 0, j)), //Y
            generateRing((i, j) -> new BlockPos(i, j, 0)), //Z
    };

    public static BlockPos[] generateRing(BiFunction<Integer, Integer, BlockPos> provider) {
        Set<BlockPos> positions = Sets.newHashSet();
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                positions.add(provider.apply(i, j));
            }
        }
        return positions.toArray(new BlockPos[8]);
    }

    public static Iterable<BlockPos.MutableBlockPos> getOffsetsAround(BlockPos center, EnumFacing facing, int distance) {
        EnumFacing.Axis axis = facing.getAxis();
        BlockPos[] positions = OFFSETS_BY_AXIS[axis.ordinal()];
        Collection<Iterable<BlockPos.MutableBlockPos>> collection = Lists.newArrayList();
        for (BlockPos pos : positions) {
            BlockPos offset = center.add(pos);
            collection.add(BlockPos.MutableBlockPos.getAllInBoxMutable(offset, offset.offset(facing, distance)));
        }
        return Iterables.concat(collection);
    }

}
